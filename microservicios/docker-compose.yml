version: '3.1'
services:
  store:
    hostname: store
    build: ./flask_app/store
    ports:
      - '13011:${GUNICORN_PORT_STORE}'
    volumes:
      - './flask_app/store:/app'
      - './utils/:/app/utils'
      - './ca:/app/ca'
    environment:
      GUNICORN_PORT: '${GUNICORN_PORT_STORE}'
      SQLALCHEMY_DATABASE_URI: '${SQLALCHEMY_DATABASE_URI_STORE}'
      SQLALCHEMY_TRACK_MODIFICATIONS: '${SQLALCHEMY_TRACK_MODIFICATIONS}'
      API_CERT_LOCATION: '${STORE_API_CERT_LOCATION}'
      API_KEY_LOCATION:  '${STORE_API_KEY_LOCATION}'
      RBT_CERT_LOCATION: '${STORE_RBT_CERT_LOCATION}'
      RBT_KEY_LOCATION: '${STORE_RBT_KEY_LOCATION}'
      RABBITMQ_CA_CERT_LOCATION: '${RABBITMQ_CA_CERT_LOCATION}'
      API_CA_LOCATION: '${API_CA_LOCATION}'
      RABBITMQ_PORT_HTTPS: '${RABBITMQ_PORT_HTTPS}'
      PYTHONUNBUFFERED: 1
      RABBITMQ_IP: '${RABBITMQ_IP}'
      CONSUL_HOST: '${CONSUL_IP}'
      SERVICE_NAME: '${STORE_SERVICE_NAME}'
      SERVICE_ID: '${STORE_SERVICE_ID}'
      LOG_LEVEL: '${LOG_LEVEL}'
    depends_on:
      - rabitmq
      - consul
    networks:
      lb7_network:
    restart: on-failure

  auth:
    hostname: auth
    build: ./flask_app/auth
    ports:
      - '13001:${GUNICORN_PORT_AUTH}'
    volumes:
      - './flask_app/auth:/app'
      - './utils/:/app/utils'
      - './ca:/app/ca'
    environment:
      GUNICORN_PORT: '${GUNICORN_PORT_AUTH}'
      SQLALCHEMY_DATABASE_URI: '${SQLALCHEMY_DATABASE_URI_AUTH}'
      SQLALCHEMY_TRACK_MODIFICATIONS: '${SQLALCHEMY_TRACK_MODIFICATIONS}'
      API_CERT_LOCATION: '${AUTH_API_CERT_LOCATION}'
      API_KEY_LOCATION: '${AUTH_API_KEY_LOCATION}'
      RBT_CERT_LOCATION: '${AUTH_RBT_CERT_LOCATION}'
      RBT_KEY_LOCATION: '${AUTH_RBT_KEY_LOCATION}'
      RABBITMQ_CA_CERT_LOCATION: '${RABBITMQ_CA_CERT_LOCATION}'
      API_CA_LOCATION: '${API_CA_LOCATION}'
      RABBITMQ_PORT_HTTPS: '${RABBITMQ_PORT_HTTPS}'
      PYTHONUNBUFFERED: 1
      RABBITMQ_IP: '${RABBITMQ_IP}'
      CONSUL_HOST: '${CONSUL_IP}'
      SERVICE_NAME: '${AUTH_SERVICE_NAME}'
      SERVICE_ID: '${AUTH_SERVICE_ID}'
      LOG_LEVEL: '${LOG_LEVEL}'
    depends_on:
      - rabitmq
      - consul
    networks:
      lb7_network:
    restart: on-failure

  delivery:
    hostname: delivery
    build: ./flask_app/delivery
    ports:
      - '13010:${GUNICORN_PORT_DELIVERY}'
    volumes:
      - './flask_app/delivery:/app'
      - './utils/:/app/utils'
      - './ca:/app/ca'
    environment:
      GUNICORN_PORT: '${GUNICORN_PORT_DELIVERY}'
      SQLALCHEMY_DATABASE_URI: '${SQLALCHEMY_DATABASE_URI_DELIVERY}'
      SQLALCHEMY_TRACK_MODIFICATIONS: '${SQLALCHEMY_TRACK_MODIFICATIONS}'
      PYTHONUNBUFFERED: 1
      RABBITMQ_IP: '${RABBITMQ_IP}'
      API_CERT_LOCATION: '${DELIVERY_API_CERT_LOCATION}'
      API_KEY_LOCATION: '${DELIVERY_API_KEY_LOCATION}'
      RBT_CERT_LOCATION: '${DELIVERY_RBT_CERT_LOCATION}'
      RBT_KEY_LOCATION: '${DELIVERY_RBT_KEY_LOCATION}'
      RABBITMQ_CA_CERT_LOCATION: '${RABBITMQ_CA_CERT_LOCATION}'
      API_CA_LOCATION: '${API_CA_LOCATION}'
      RABBITMQ_PORT_HTTPS: '${RABBITMQ_PORT_HTTPS}'
      HAPROXY_IP: haproxy
      HAPROXY_PORT: '${HAPROXY_PORT}'
      CONSUL_HOST: '${CONSUL_IP}'
      SERVICE_NAME: '${DELIVERY_SERVICE_NAME}'
      SERVICE_ID: '${DELIVERY_SERVICE_ID}'
      LOG_LEVEL: '${LOG_LEVEL}'
    depends_on:
      - rabitmq
    networks:
      lb7_network:
        ipv4_address: '${SERVICE4_IP}'
    restart: on-failure
  machine:
    hostname: machine
    build: ./flask_app/machine
    ports:
      - '13005:${GUNICORN_PORT_MACHINE}'
    volumes:
      - './flask_app/machine:/app'
      - './utils/:/app/utils'
      - './ca:/app/ca'
    environment:
      GUNICORN_PORT: '${GUNICORN_PORT_MACHINE}'
      PYTHONUNBUFFERED: 1
      RABBITMQ_IP: '${RABBITMQ_IP}'
      API_CERT_LOCATION: '${MACHINE_API_CERT_LOCATION}'
      API_KEY_LOCATION: '${MACHINE_API_KEY_LOCATION}'
      RBT_CERT_LOCATION: '${MACHINE_RBT_CERT_LOCATION}'
      RBT_KEY_LOCATION: '${MACHINE_RBT_KEY_LOCATION}'
      RABBITMQ_CA_CERT_LOCATION: '${RABBITMQ_CA_CERT_LOCATION}'
      API_CA_LOCATION: '${API_CA_LOCATION}'
      RABBITMQ_PORT_HTTPS: '${RABBITMQ_PORT_HTTPS}'
      HAPROXY_IP: haproxy
      HAPROXY_PORT: '${HAPROXY_PORT}'
      CONSUL_HOST: '${CONSUL_IP}'
      SERVICE_NAME: '${MACHINE_SERVICE_NAME}'
      SERVICE_ID: '${MACHINE_SERVICE_ID}'
      LOG_LEVEL: '${LOG_LEVEL}'

    depends_on:
      - rabitmq
    networks:
      lb7_network:
    restart: on-failure

  order:
    hostname: order
    build: ./flask_app/order
    volumes:
      - './flask_app/order:/app'
      - './utils/:/app/utils'
      - './ca:/app/ca'
    environment:
      GUNICORN_PORT: '${GUNICORN_PORT_ORDER}'
      SQLALCHEMY_DATABASE_URI: '${SQLALCHEMY_DATABASE_URI_ORDER}'
      SQLALCHEMY_TRACK_MODIFICATIONS: '${SQLALCHEMY_TRACK_MODIFICATIONS}'
      PYTHONUNBUFFERED: 1
      RABBITMQ_IP: '${RABBITMQ_IP}'
      API_CERT_LOCATION: '${ORDER_API_CERT_LOCATION}'
      API_KEY_LOCATION: '${ORDER_API_KEY_LOCATION}'
      RBT_CERT_LOCATION: '${ORDER_RBT_CERT_LOCATION}'
      RBT_KEY_LOCATION: '${ORDER_RBT_KEY_LOCATION}'
      RABBITMQ_CA_CERT_LOCATION: '${RABBITMQ_CA_CERT_LOCATION}'
      API_CA_LOCATION: '${API_CA_LOCATION}'
      RABBITMQ_PORT_HTTPS: '${RABBITMQ_PORT_HTTPS}'
      HAPROXY_IP: haproxy
      HAPROXY_PORT: '${HAPROXY_PORT}'
      CONSUL_HOST: '${CONSUL_IP}'
      SERVICE_NAME: '${ORDER_SERVICE_NAME}'
      SERVICE_ID: '${ORDER_SERVICE_ID}'
      LOG_LEVEL: '${LOG_LEVEL}'

    depends_on:
      - rabitmq
    networks:
      lb7_network:
    restart: on-failure

  log:
    hostname: log
    build: ./flask_app/log_service
    ports:
      - '13006:${GUNICORN_PORT_LOG}'
    volumes:
      - './flask_app/log_service:/app'
      - './utils/:/app/utils'
      - './ca:/app/ca'
    environment:
      GUNICORN_PORT: '${GUNICORN_PORT_LOG}'
      SQLALCHEMY_DATABASE_URI: '${SQLALCHEMY_DATABASE_URI_LOG}'
      SQLALCHEMY_TRACK_MODIFICATIONS: '${SQLALCHEMY_TRACK_MODIFICATIONS}'
      PYTHONUNBUFFERED: 1
      RABBITMQ_IP: '${RABBITMQ_IP}'
      API_CERT_LOCATION: '${LOG_API_CERT_LOCATION}'
      API_KEY_LOCATION: '${LOG_API_KEY_LOCATION}'
      RBT_CERT_LOCATION: '${LOG_RBT_CERT_LOCATION}'
      RBT_KEY_LOCATION: '${LOG_RBT_KEY_LOCATION}'
      RABBITMQ_CA_CERT_LOCATION: '${RABBITMQ_CA_CERT_LOCATION}'
      API_CA_LOCATION: '${API_CA_LOCATION}'
      RABBITMQ_PORT_HTTPS: '${RABBITMQ_PORT_HTTPS}'
      HAPROXY_IP: haproxy
      HAPROXY_PORT: '${HAPROXY_PORT}'
      CONSUL_HOST: '${CONSUL_IP}'
      SERVICE_NAME: '${LOG_SERVICE_NAME}'
      SERVICE_ID: '${LOG_SERVICE_ID}'
      LOG_LEVEL: '${LOG_LEVEL}'

    depends_on:
      - rabitmq
    networks:
      lb7_network:
        ipv4_address: '${SERVICE7_IP}'
    restart: on-failure
  payment:
    hostname: payment
    build: ./flask_app/payment
    ports:
      - '13002:${GUNICORN_PORT_PAYMENT}'
    volumes:
      - './flask_app/payment:/app'
      - './utils/:/app/utils'
      - './ca:/app/ca'
    environment:
      GUNICORN_PORT: '${GUNICORN_PORT_PAYMENT}'
      SQLALCHEMY_DATABASE_URI: '${SQLALCHEMY_DATABASE_URI_PAYMENT}'
      SQLALCHEMY_TRACK_MODIFICATIONS: '${SQLALCHEMY_TRACK_MODIFICATIONS}'
      PYTHONUNBUFFERED: 1
      RABBITMQ_IP: '${RABBITMQ_IP}'
      API_CERT_LOCATION: '${PAYMENT_API_CERT_LOCATION}'
      API_KEY_LOCATION: '${PAYMENT_API_KEY_LOCATION}'
      RBT_CERT_LOCATION: '${PAYMENT_RBT_CERT_LOCATION}'
      RBT_KEY_LOCATION: '${PAYMENT_RBT_KEY_LOCATION}'
      RABBITMQ_CA_CERT_LOCATION: '${RABBITMQ_CA_CERT_LOCATION}'
      API_CA_LOCATION: '${API_CA_LOCATION}'
      RABBITMQ_PORT_HTTPS: '${RABBITMQ_PORT_HTTPS}'
      HAPROXY_IP: haproxy
      HAPROXY_PORT: '${HAPROXY_PORT}'
      CONSUL_HOST: '${CONSUL_IP}'
      SERVICE_NAME: '${PAYMENT_SERVICE_NAME}'
      SERVICE_ID: '${PAYMENT_SERVICE_ID}'
      LOG_LEVEL: '${LOG_LEVEL}'

    depends_on:
      - rabitmq
    networks:
      lb7_network:
    restart: on-failure

  haproxy:
    build: ./haproxy
    volumes:
      - './ca:/usr/local/etc/haproxy/ca'
    ports:
      - '${HAPROXY_PORT}:${HAPROXY_PORT}'
      - '${HAPROXY_STATS_PORT}:${HAPROXY_STATS_PORT}'
    expose:
      - '${HAPROXY_PORT}'
      - '${HAPROXY_STATS_PORT}'
    networks:
      lb7_network:
        ipv4_address: '${HAPROXY_IP}'
    environment:
      SERVICE1_IP: '${SERVICE1_IP}'
      SERVICE1_PORT: '${GUNICORN_PORT_PAYMENT}'
      SERVICE2_IP: '${SERVICE2_IP}'
      SERVICE2_PORT: '${GUNICORN_PORT_AUTH}'
      SERVICE3_IP: '${SERVICE3_IP}'
      SERVICE3_PORT: '${GUNICORN_PORT_MACHINE}'
      SERVICE4_IP: '${SERVICE4_IP}'
      SERVICE4_PORT: '${GUNICORN_PORT_DELIVERY}'
      SERVICE5_IP: '${SERVICE5_IP}'
      SERVICE5_PORT: '${GUNICORN_PORT_ORDER}'
      SERVICE7_PORT: '${GUNICORN_PORT_LOG}'
      SERVICE7_IP: '${SERVICE7_IP}'
      HAPROXY_PORT: '${HAPROXY_PORT}'
      HAPROXY_STATS_PORT: '${HAPROXY_STATS_PORT}'
      CONSUL_HOST: '${CONSUL_IP}'
    restart: on-failure
  rabitmq:
    image: 'rabbitmq:3-management'
    networks:
      lb7_network:
        ipv4_address: '${RABBITMQ_IP}'
    volumes:
      - './rabbitmq:/conf'
      - './ca:/conf/ca'
    ports:
      - '5672:5672'
      - '5671:5671'
      - '15672:15672'
      - '15671:15671'
    environment:
      - 'RABBITMQ_SSL_CACERTFILE=${RABBITMQ_SSL_CACERTFILE}'
      - 'RABBITMQ_SSL_CERTFILE=${RABBITMQ_SSL_CERTFILE}'
      - 'RABBITMQ_SSL_KEYFILE=${RABBITMQ_SSL_KEYFILE}'
      - 'RABBITMQ_SSL_FAIL_IF_NO_PEER_CERT=${RABBITMQ_SSL_FAIL_IF_NO_PEER_CERT}'
      - 'RABBITMQ_SSL_VERIFY=${RABBITMQ_SSL_VERIFY}'

  consul:
    image: 'consul:latest'
    volumes:
      - './consul:/consul/config'
    ports:
      - '8300:8300'
      - '8301:8301'
      - '8301:8301/udp'
      - '8500:8500'
      - '8501:8501'
      - '8600:8600'
      - '8600:8600/udp'
    networks:
      lb7_network:
        ipv4_address: '${CONSUL_IP}'

networks:
  lb7_network:
    driver: bridge
    ipam:
      driver: default
      config:
        - subnet: '${NETWORK_SUBNET}'
