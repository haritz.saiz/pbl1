import jwt

from utils.logger import Logger

logger = Logger("businessLogic")

class BusinessLogic():
    __instance = None
    __public_key = None
    __up_status = False

    def __init__(self):
        if BusinessLogic.__instance is not None:
            raise Exception("This class is a singelton")
        else:
            message = "Service initialized"
            logger.print(message, Logger.INFO)
            BusinessLogic.__instance = self


    @staticmethod
    def get_instance():
        if BusinessLogic.__instance is None:
            BusinessLogic()
        return BusinessLogic.__instance

    def decrypt_jwt(self, auth_header):
        args = auth_header.split("Bearer ")
        jwt_token = args[1]
        decoded_token = jwt.decode(jwt_token, BusinessLogic.__public_key, algorithms='RS256')
        return decoded_token

    def set_up_status(self, status):
        BusinessLogic.__up_status = status
        if status:
            message = "Service up"
            logger.print(message, level=Logger.INFO)

    def get_auth_public_key(self):
        return BusinessLogic.__public_key

    def set_auth_public_key(self, pubkey):
        BusinessLogic.__public_key = pubkey

    def set_up_status(self, status):
        BusinessLogic.__up_status = status

    def get_up_status(self):
        return BusinessLogic.__up_status
