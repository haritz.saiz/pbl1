from threading import Event

from .models import Order, Sagas
from . import Session, Logger
import jwt

logger = Logger("businessLogic")

class BusinessLogic():
    __instance = None
    __public_key = None
    __up_status = False

    def __init__(self):
        if BusinessLogic.__instance is not None:
            raise Exception("This class is a singelton")
        else:
            message = "Service initialized"
            logger.print(message, Logger.INFO)
            BusinessLogic.__instance = self


    @staticmethod
    def get_instance():
        if BusinessLogic.__instance is None:
            BusinessLogic()
        return BusinessLogic.__instance

    def decrypt_jwt(self, auth_header):
        args = auth_header.split("Bearer ")
        jwt_token = args[1]
        decoded_token = jwt.decode(jwt_token, BusinessLogic.__public_key, algorithms='RS256')
        return decoded_token

    def set_auth_public_key(self, pubkey):
        BusinessLogic.__public_key = pubkey

    def get_auth_public_key(self):
        return BusinessLogic.__public_key

    def set_up_status(self, status):
        BusinessLogic.__up_status = status
        if status:
            message = "Service up"
            logger.print(message, level=Logger.INFO)

    def get_up_status(self):
        return BusinessLogic.__up_status

    def create_order(self, client_id, number_of_pieces, type):
        session = Session()
        try:
            new_order = Order(
                number_of_pieces=number_of_pieces,
                client_id=client_id,
                status=Order.STATUS_INIT,
                type=type,
            )

            session.add(new_order)
            session.commit()
            new_order_dict = new_order.as_dict()
            session.close()
            return new_order_dict
        except KeyError:
            session.rollback()
            session.close()
            return None


    def get_all_order(self):
        session = Session()
        orders = session.query(Order).all()
        session.close()
        return orders

    def get_order(self, order_id):
        session = Session()
        order = session.query(Order).get(order_id)
        if not order:
            session.close()
            return None
        session.close()
        return order

    def update_order(self, order_id, status):
        logger.print("Updating order: " + str(order_id) + " to status: " + status, Logger.INFO)
        session = Session()
        order = session.query(Order).get(order_id)
        if not order:
            return None
        order.status = status
        session.commit()
        order_dict = order.as_dict()
        session.close()
        return order_dict

    def cancel_request_order (self, order_id):
        session = Session()
        order = session.query(Order).get(order_id)

        if not order:
            return None
        order.cancel_request = True

        session.commit()
        session.close()
        return True

    def update_lock(self, order_id, lock):
        logger.print("Updating order lock: " + str(order_id) + " to lock: " + str(lock), Logger.INFO)
        session = Session()
        order = session.query(Order).get(order_id)

        if not order:
            return None
        order.lock = lock

        session.commit()
        session.close()
        return True

    def update_delivery_id(self, order_id, delivery_id):
        session = Session()
        try:
            order = session.query(Order).get(order_id)
            order.delivery_id = delivery_id
            session.commit()
            session.close()
        except KeyError:
            session.rollback()
            session.close()

    def create_sagas(self, order_id, sagas):
        session = Session()
        try:
            order = session.query(Order).get(order_id)
            sagas.order = order
            session.add(sagas)
            session.commit()
            sagas_dict = sagas.as_dict()
            session.close()
            return sagas_dict
        except KeyError:
            session.rollback()
            session.close()
            return None

    def update_sagas(self, sagas_id, status):
        session = Session()
        try:
            sagas = session.query(Sagas).get(sagas_id)
            sagas.status = status
            session.commit()
            session.close()
        except KeyError:
            session.rollback()
            session.close()