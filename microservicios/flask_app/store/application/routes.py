from flask import request, jsonify, abort
from flask import current_app as app
from jwt import DecodeError, ExpiredSignatureError
from werkzeug.exceptions import NotFound, InternalServerError, BadRequest, UnsupportedMediaType, HTTPException, \
    NotAcceptable, Unauthorized, ServiceUnavailable
import traceback

from .store import StoreHouse
from .businessLogic import BusinessLogic
from utils.EventPublisher import EventPublisher
from utils.logger import Logger
from utils.config import Config
from utils.BLConsul import BLConsul
from .models import Piece

logger = Logger("routes")

config = Config.get_instance()
bl_consul = BLConsul.get_instance()

eventPublisher = EventPublisher("store", "fanout")

# Store Routes #########################################################################################################

@app.route('/piece/<int:id>', methods=['PUT'])
def update_piece(id):
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "U_PIECE" in decoded_token["perms"]:
            if request.headers['Content-Type'] != 'application/json':
                logger.print(msg="Unsupported media type in update_piece", level=Logger.ERROR)
                abort(UnsupportedMediaType.code)

            content = request.json
            status = content["status"]
            piece_dict = BusinessLogic.get_instance().update_piece(id, status)
            if piece_dict is None:
                logger.print(msg="Bad request in update_piece", level=Logger.ERROR)
                abort(BadRequest.code)
            else:
                logger.print(msg="PUT /piece/<int:id>",  level=Logger.DEBUG)
                response = jsonify(piece_dict)
            return response
        else:
            logger.print(msg="Not enough permissions in update_piece",  level=Logger.ERROR)
            abort(Unauthorized.code)

    except (KeyError, DecodeError, ExpiredSignatureError):
        logger.print(msg="Token error in update_piece", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route('/piece', methods=['GET'])
@app.route('/pieces', methods=['GET'])
def view_pieces():
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "R_PIECE" in decoded_token["perms"]:
            pieces = BusinessLogic.get_instance().get_all_pieces()
            response = jsonify(Piece.list_as_dict(pieces))
            logger.print(msg="GET /piece /pieces", level=Logger.DEBUG)
            return response
        else:
            logger.print(msg="Not enough permissions in view_pieces",  level=Logger.ERROR)
            abort(Unauthorized.code)

    except (KeyError, DecodeError, ExpiredSignatureError):
        logger.print(msg="Token error in view_pieces", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route('/piece/<int:piece_ref>', methods=['GET'])
def view_piece(piece_ref):
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "R_PIECE" in decoded_token["perms"]:
            piece = BusinessLogic.get_instance().get_piece(piece_ref)
            if piece is None:
                logger.print(msg="Not found in view_piece",  level=Logger.ERROR)
                abort(NotFound.code)
            logger.print(msg="GET /piece/<int:piece_ref>", level=Logger.DEBUG)
            response = jsonify(piece.as_dict())
            return response
        else:
            logger.print(msg="Not enough permissions in view_piece", level=Logger.ERROR)
            abort(Unauthorized.code)

    except (KeyError, DecodeError, ExpiredSignatureError):
        logger.print(msg="Token error in view_piece", level=Logger.ERROR)
        abort(Unauthorized.code)

@app.route('/store/status', methods=['GET'])
def view_store():
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "R_STORE" in decoded_token["perms"]:
            store = StoreHouse.get_instance()
            store_status = {
                "status": store.status,
                "order_list": str(store.queue_order),
                "piece_list": str(store.queue_pieces)
            }

            logger.print(msg="GET /piece/<int:piece_ref>", level=Logger.DEBUG)
            response = store_status
            return response
        else:
            logger.print(msg="Not enough permissions in view_piece", level=Logger.ERROR)
            abort(Unauthorized.code)

    except (KeyError, DecodeError, ExpiredSignatureError):
        logger.print(msg="Token error in view_piece", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route('/store/health', methods=['HEAD', 'GET'])
@app.route('/health', methods=['HEAD', 'GET'])
def health_check():
    if BusinessLogic.get_instance().get_up_status() == False:
        logger.print(msg="Service unavailable",  level=Logger.ERROR)
        abort(ServiceUnavailable)

    logger.print(msg="GET /health", level=Logger.HEALTH)
    return "OK"

# Error Handling #######################################################################################################
@app.errorhandler(UnsupportedMediaType)
def unsupported_media_type_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(BadRequest)
def bad_request_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(NotFound)
def resource_not_found_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(InternalServerError)
def server_error_handler(e):
    return get_jsonified_error(e)


def get_jsonified_error(e):
    logger.print(str({"error_code": e.code, "error_message": e.description, "request": request}), level=Logger.ERROR)
    return jsonify({"error_code": e.code, "error_message": e.description}), e.code