from random import randint
from time import sleep
from collections import deque
from threading import Thread, Lock, Event

from .models import Piece
from .businessLogic import BusinessLogic
from utils.EventPublisher import EventPublisher
import json
from utils.logger import Logger

logger = Logger("store")
eventPublisherOrder = EventPublisher("order_performer", "fanout")

class StoreHouse(Thread):
    __instance = None

    MINIMUM_STOCK_PER_TYPE = 3
    PIECE_TYPES = ["type_a", "type_b", "type_c"]

    STATUS_WAITING = "Waiting"
    STATUS_WORKING = "Working"
    __stauslock__ = Lock()
    thread_session = None

    def __init__(self):
        if StoreHouse.__instance is not None:
            raise Exception("This class is a singelton")
        else:
            StoreHouse.__instance = self
            Thread.__init__(self)
            self.queue_order = deque([])
            self.queue_pieces = BusinessLogic.get_instance().get_manufactured_pieces()
            self.working_piece = None
            self.working_order = None
            self.status = StoreHouse.STATUS_WAITING
            self.instance = self
            self.queue_order_not_empty_event = Event()
            self.queue_pieces_not_empty_event = Event()
            #self.reload_pieces_at_startup()
            if self.queue_pieces is None:
                self.queue_pieces = []
            logger.print("(" + str(len(self.queue_pieces)) + ") Inital piece queue: " + str(self.queue_pieces), Logger.DEBUG)
            self.start()

    @staticmethod
    def get_instance():
        if StoreHouse.__instance is None:
            StoreHouse()
        return StoreHouse.__instance

    def produce_minimum_stock(self):
        for type in StoreHouse.PIECE_TYPES:
            num_pieces = len(self.filter_piece_queue_by_type(type))
            if num_pieces < StoreHouse.MINIMUM_STOCK_PER_TYPE:
                pieces_to_produce = StoreHouse.MINIMUM_STOCK_PER_TYPE - num_pieces
                self.order_piece_production(pieces_to_produce, type)

    def run(self):

        self.produce_minimum_stock()

        while True:
            self.queue_order_not_empty_event.wait()
            self.instance.status = StoreHouse.STATUS_WORKING
            logger.print("Thread notified that queue order is not empty.", level=Logger.INFO)

            while self.queue_order.__len__() > 0:
                self.instance.process_first_order()

            self.queue_order_not_empty_event.clear()
            self.produce_minimum_stock()
            logger.print("Lock thread because query order is empty.", level=Logger.INFO)

            self.instance.status = StoreHouse.STATUS_WAITING


    def process_first_order(self):
        # Get order from queue
        order = self.queue_order.popleft()
        self.update_order_status(order, "Being manufactured")

        logger.print(msg="Processing order: " + str(order["order_id"]),  level=Logger.INFO)

        self.working_order = order

        pieces = self.filter_piece_queue_by_type(order["type"])
        logger.print(msg="Checking available pieces",  level=Logger.INFO)
        if len(pieces) < order["number_of_pieces"]:
            logger.print(msg="Not enough pieces, sending production request",  level=Logger.INFO)
            dif = order["number_of_pieces"] - len(pieces)
            self.order_piece_production(dif, order["type"])
            logger.print(msg="Waiting for pieces ...", level=Logger.INFO)
            self.queue_pieces_not_empty_event.wait()
            pieces = self.filter_piece_queue_by_type(order["type"])

        logger.print(msg="Assigning Owner..", level=Logger.INFO)
        #asignar las piezas al order
        for piece in pieces:
            BusinessLogic.get_instance().update_piece_owner(piece.id, order["order_id"])
            logger.print(msg="Piece with id: " + str(piece.id) + " owned by order: " + str(order["order_id"]), level=Logger.INFO)

        logger.print(msg="Piece list: " + str(pieces),
                     level=Logger.INFO)

        #borrar las piezas de la cola
        self.remove_pieces_from_queue(pieces)

        #terminar order
        logger.print(msg="Order: " + str(order["order_id"]) + " finished",
                     level=Logger.INFO)
        self.update_order_status(order, "Created")


    def order_piece_production(self, number_of_pieces, type):
        eventPublisher = EventPublisher("machine", "fanout")
        for i in range(number_of_pieces):
            #Crea una pieza -> Status = Created
            piece = BusinessLogic.get_instance().create_piece(type)
            eventPublisher.send_data(json.dumps({"piece_id": piece["id"], "type": type}), "")
            BusinessLogic.get_instance().update_piece_status(piece["id"], Piece.STATUS_MANUFACTURING)

    def add_order_to_queue(self, order):
        self.queue_order.append(order)
        logger.print("Adding order to queue: {}".format(order["order_id"]), level=Logger.INFO)
        self.update_order_status(order, "Queued to store")
        self.queue_order_not_empty_event.set()

    def add_piece_to_queue(self, piece):
        self.queue_pieces.append(piece)
        logger.print("Adding piece to queue: {}".format(piece.id), level=Logger.INFO)
        if self.working_order is not None:
            if len(self.filter_piece_queue_by_type(self.working_order["type"])) >= self.working_order["number_of_pieces"]:
                self.queue_pieces_not_empty_event.set()

    def filter_piece_queue_by_type(self, type):
        logger.print(".........Filtering piece queue.........", level=Logger.DEBUG)
        queue = []
        for piece in self.queue_pieces:
            logger.print("item: " + str(piece) + "    type = " + type, level=Logger.DEBUG)
            if piece.type == type:
                queue.append(piece)

        logger.print("Queue final: " + str(queue), level=Logger.DEBUG)
        logger.print(".........Filtered piece queue.........", level=Logger.DEBUG)
        return queue

    def contains_piece(self, piece_id, pieces):
        logger.print(".........Contains piece queue.........", level=Logger.DEBUG)
        for piece in pieces:
            logger.print("piece: " + str(piece_id) + "     list: "+str(pieces), level=Logger.DEBUG)
            if piece.id == piece_id:
                logger.print(".........Contains piece queue END => Found .........", level=Logger.DEBUG)
                return True
        logger.print(".........Contains piece queue END => Not Found .........", level=Logger.DEBUG)
        return False

    def remove_pieces_from_queue(self, pieces):
        logger.print(".........Remove piece queue. Items to be removed => " + str(pieces) +".........", level=Logger.DEBUG)
        logger.print("Queue initial: " + str(self.queue_pieces), level=Logger.DEBUG)
        i = 0
        loop = True
        if len(self.queue_pieces) > 0:
            while loop:
                piece = self.queue_pieces[i]
                logger.print("i= : " + str(i) + " len: " + str(len(self.queue_pieces)), level=Logger.DEBUG)
                logger.print("item: " + str(piece.id), level=Logger.DEBUG)
                if self.contains_piece(piece.id, pieces):
                    logger.print("Item found", level=Logger.DEBUG)
                    del self.queue_pieces[i]

                if i < len(self.queue_pieces)-1:
                    i = i + 1
                else:
                    loop = False

        logger.print(".........Remove piece queue END .........", level=Logger.DEBUG)

    def remove_order_from_queue(self, order_id):
        logger.print(".........Remove order from queue. Order to be removed => " + str(order_id) +".........", level=Logger.DEBUG)
        logger.print("Queue initial: " + str(self.queue_order), level=Logger.DEBUG)
        i = 0
        loop = True
        if len(self.queue_order) > 0:

            while loop:
                order_id_queue = self.queue_order[i]
                logger.print("i= : " + str(i) + " len: " + str(len(self.queue_order)), level=Logger.DEBUG)
                logger.print("item: " + str(order_id_queue), level=Logger.DEBUG)
                if order_id_queue == order_id:
                    logger.print("Item found", level=Logger.DEBUG)
                    if self.working_order == order_id:
                        logger.print("Cannot delete order as it is being procesed", level=Logger.DEBUG)
                    else:
                        del self.queue_order[i]

                if i < len(self.queue_order)-1:
                    i = i + 1
                else:
                    loop = False

        logger.print(".........Remove order queue END .........", level=Logger.DEBUG)

    def update_order_status(self, order, status):
        eventPublisherOrder.send_data(json.dumps({
            "order_id": order["order_id"],
            "status": status
        }), "")