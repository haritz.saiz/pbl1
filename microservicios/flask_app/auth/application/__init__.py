import json

from flask import Flask
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import create_engine
from utils.BLConsul import BLConsul
from utils.config import Config
from utils.logger import Logger
logger = Logger("__init__")

engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)
Session = scoped_session(
            sessionmaker(
                autocommit=False,
                autoflush=True,
                bind=engine)
        )


def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)

    with app.app_context():
        from . import routes
        from . import models
        from .businessLogic import BusinessLogic
        from utils.EventPublisher import EventPublisher

        models.Base.metadata.create_all(engine)
        BusinessLogic.get_instance().set_auth_public_key(BusinessLogic.get_instance().get_public_key())
        eventPublisher = EventPublisher(exchange="auth_pubkey", type="fanout")
        eventPublisher.send_data(json.dumps({
            "key": BusinessLogic.get_instance().get_public_key()
        }), "")
        try:
            logger.print(msg="Creating admin user and roles", level=Logger.INFO)
            bl = BusinessLogic.get_instance()
            bl.create_user("admin", "admin")
            bl.create_role("ADMIN_ROLE")
            bl.assign_role_to_user("ADMIN_ROLE", "admin")
            services_to_create = ["ORDER", "AUTH", "LOG", "DELIVERY", "PAYMENT", "MACHINE", "PIECE", "STORE"]
            perms = ["C", "R", "U", "D"]
            for serv in services_to_create:
                bl.create_service(serv)
                for perm in perms:
                    perm_name = perm + "_" + serv
                    bl.create_permission(perm_name, serv)
                    bl.assign_permission_to_role(perm_name, "ADMIN_ROLE")

            bl.create_role("USER_ROLE")
            bl.assign_permission_to_role("C_ORDER", "USER_ROLE")
            bl.assign_permission_to_role("R_ORDER", "USER_ROLE")
            bl.assign_permission_to_role("D_ORDER", "USER_ROLE")
            bl.assign_permission_to_role("C_PAYMENT", "USER_ROLE")
            bl.assign_permission_to_role("R_PAYMENT", "USER_ROLE")
            bl.assign_permission_to_role("U_PAYMENT", "USER_ROLE")
            bl.assign_permission_to_role("R_DELIVERY", "USER_ROLE")
        except:
            logger.print(msg="Admin and roles already created", level=Logger.INFO)
            Session.rollback()
        """
        bl.assign_role_to_user("ADMIN_ROLE", "admin")
        bl.create_service("ORDER")
        bl.create_permission("CREATE_1", "ORDER")
        bl.assign_permission_to_role("CREATE_1", "ADMIN_ROLE")
        print(bl.get_user_roles("admin"))
        print(bl.get_role_permissions("ADMIN_ROLE"))
        print(bl.get_permission("CREATE_1"))
        bl.create_permission("CREATE_2", "ORDER")
        bl.assign_permission_to_role("CREATE_2", "ADMIN_ROLE2")
        print(bl.get_user_permissions("admin"))
        """
        bl_consul = BLConsul.get_instance()
        bl_consul.init_and_register(app)

        logger.print(msg="Init phase finalized", level=Logger.INFO)
        BusinessLogic.get_instance().set_up_status(True)
        return app
