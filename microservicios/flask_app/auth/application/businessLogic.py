from utils.logger import Logger
from .models import User, Service, Permission, Role
from . import Session
from Crypto.PublicKey import RSA
import bcrypt
import copy
import jwt

logger = Logger("businessLogic")


class BusinessLogic:
    __instance = None
    __public_key = None
    __up_status = False

    def __init__(self):
        if BusinessLogic.__instance is not None:
            raise Exception("This class is a singelton")
        else:
            message = "Service initialized"
            logger.print(message, Logger.INFO)
            BusinessLogic.__instance = self


    @staticmethod
    def get_instance():
        if BusinessLogic.__instance is None:
            BusinessLogic()
        return BusinessLogic.__instance

    def decrypt_jwt(self, auth_header):
        args = auth_header.split("Bearer ")
        jwt_token = args[1]
        decoded_token = jwt.decode(jwt_token, BusinessLogic.__public_key, algorithms='RS256')
        return decoded_token

    def set_auth_public_key(self, pubkey):
        BusinessLogic.__public_key = pubkey

    def get_auth_public_key(self):
        return BusinessLogic.__public_key

    def set_up_status(self, status):
        BusinessLogic.__up_status = status
        if status:
            message = "Service up"
            logger.print(message, level=Logger.INFO)

    def get_up_status(self):
        return BusinessLogic.__up_status

    def create_user(self, username, password):
        session = Session()
        new_user = None
        try:
            new_user = User(
                username=username,
                password=bcrypt.hashpw(password.encode(), bcrypt.gensalt()).decode()
            )

            session.add(new_user)
            session.commit()
            new_user_dict = new_user.as_dict()
            session.close()
            return new_user_dict
        except KeyError:
            session.rollback()
            session.close()
            return None


    def get_all_users(self):
        session = Session()
        users = session.query(User).all()
        session.close()
        return users

    def get_user(self, username):
        session = Session()
        user = session.query(User).get(username)
        if not user:
            session.close()
            return None
        session.close()
        return user


    def get_public_key(self):
        secret_code = "Monolitics"
        encoded_key = open("rsa_key.pem", "rb").read()
        key_dec = RSA.import_key(encoded_key, passphrase=secret_code)
        return key_dec.publickey().export_key().decode()

    def check_user(self, username, password):
        session = Session()
        user = session.query(User).get(username)
        if not user:
            session.close()
            return None
        if not bcrypt.checkpw(password.encode(), user.password.encode()):
            session.close()
            return None
        session.close()
        return user

    def get_private_key(self):
        secret_code = "Monolitics"
        encoded_key = open("rsa_key.pem", "rb").read()
        key_dec = RSA.import_key(encoded_key, passphrase=secret_code)
        return key_dec.export_key().decode()

    def create_role(self, name):
        session = Session()
        try:
            role = Role(
                role_name=name,
            )
            session.add(role)
            session.commit()
            role_as_dict = role.as_dict()
            session.close()
            return role_as_dict
        except KeyError:
            session.rollback()
            session.close()
            return None
        return

    def create_permission(self, type, serviceName):
        session = Session()
        try:
            service = session.query(Service).get(serviceName)
            perm = Permission(
                permission_name=type,
                permission_name_backref = service
            )
            session.add(perm)
            session.commit()
            perm_as_dict = perm.as_dict()
            session.close()
            return perm_as_dict
        except KeyError:
            session.rollback()
            session.close()
            return None
        return

    def create_service(self, name):
        session = Session()
        try:
            service = Service(
                service_name=name,
            )
            session.add(service)
            session.commit()
            service_as_dict = service.as_dict()
            session.close()
            return service_as_dict
        except KeyError:
            session.rollback()
            session.close()
            return None
        return

    def assign_role_to_user(self, roleName, username):
        session = Session()
        try:
            role = session.query(Role).get(roleName)
            user = session.query(User).get(username)
            role.users_roles.append(user)
            session.commit()
            session.close()
            return True
        except KeyError:
            session.rollback()
            session.close()
            return None
        return

    def assign_permission_to_role(self, permission_name, role):
        session = Session()
        try:
            role = session.query(Role).get(role)
            permission = session.query(Permission).get(permission_name)
            permission.roles_permissions.append(role)
            session.commit()
            session.close()
            return True
        except KeyError:
            session.rollback()
            session.close()
            return None
        return

    def get_user_roles(self, username):
        session = Session()
        try:
            user = session.query(User).get(username)
            roles = []
            for role in user.user_roles_relation:
                roles.append(role.role_name)
            session.commit()
            session.close()
            return roles
        except KeyError:
            session.rollback()
            session.close()
            return None
        return

    def get_role_permissions(self, role_name):
        session = Session()
        try:
            role = session.query(Role).get(role_name)
            perms = []
            for perm in role.roles_permissions_relation:
                perms.append(perm.permission_name)
            session.commit()
            session.close()
            return perms
        except KeyError:
            session.rollback()
            session.close()
            return None
        return

    def get_user_permissions(self, username):
        perms = []
        bl = BusinessLogic.get_instance()
        roles = bl.get_user_roles(username)
        for role in roles:
            perms.append(bl.get_role_permissions(role))

        flat_list = [item for sublist in perms for item in sublist]
        return flat_list

    def get_role(self, role_name):
        session = Session()
        try:
            role = session.query(Role).get(role_name)
            session.commit()
            session.close()
            return role
        except KeyError:
            session.rollback()
            session.close()
            return None
        return

    def get_permission(self, permission_name):
        session = Session()
        try:
            perm = session.query(Permission).get(permission_name)
            perm_copy = copy.deepcopy(perm)
            session.commit()
            session.close()
            return perm_copy
        except KeyError:
            session.rollback()
            session.close()
            return None
        return

    def get_service(self, service_name):
        session = Session()
        try:
            service = session.query(Service).get(service_name)
            session.commit()
            session.close()
            return service
        except KeyError:
            session.rollback()
            session.close()
            return None
        return