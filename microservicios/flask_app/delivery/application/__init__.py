import time

from flask import Flask
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import create_engine

from utils.EventPublisher import EventPublisher
from .models import Delivery
from utils.config import Config
from .api_client import get_auth_public_key
from utils.logger import Logger
from utils.BLConsul import BLConsul

logger = Logger("__init__")

engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)
Session = scoped_session(
            sessionmaker(
                autocommit=False,
                autoflush=True,
                bind=engine)
)

from .businessLogic import BusinessLogic
from .delivery_performer import *

def callbackCheckOrder(msgRaw):
    jsonMsg = json.loads(msgRaw)
    new_delivery_dict = BusinessLogic.get_instance().create_delivery(jsonMsg["order_id"], jsonMsg["client_id"],
                                                                     jsonMsg["address"])
    if new_delivery_dict is not None and new_delivery_dict["address"] in Delivery.ATTEND_CODES:
        event = dict(
            status="delivery_done",
            delivery_id=new_delivery_dict["id"]
        )
        logger.print(msg="event: delivery done for client id "+str(jsonMsg["client_id"]) + " in order id "+str(jsonMsg["order_id"]), level=Logger.INFO)
    else:
        event = dict(
            status="delivery_error",
        )
        logger.print(msg="event: delivery error for client id "+str(jsonMsg["client_id"]) + " in order id "+str(jsonMsg["order_id"]), level=Logger.INFO)

    exchange_response = jsonMsg["exchange_response"]
    eventPublisher = EventPublisher(exchange_response, "fanout")
    eventPublisher.send_data(json.dumps({"event": event}), "")


from .businessLogic import BusinessLogic
def callbackUpdatePubkey(msgRaw):
    jsonMsg = json.loads(msgRaw)
    BusinessLogic.get_instance().set_auth_public_key(jsonMsg["key"])

def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)

    with app.app_context():
        from . import routes
        from . import models

        from utils.EventHandler import EventHandler

        while True:
            logger.print(msg="Trying getting Pubkey", level=Logger.DEBUG)
            pub_key_response = get_auth_public_key()
            if pub_key_response is None:
                logger.print(msg="Error getting Auth Pubkey", level=Logger.DEBUG)
                time.sleep(20)
                continue

            logger.print(msg="Pubkey successfully getted", level=Logger.DEBUG)
            break

        BusinessLogic.get_instance().set_auth_public_key(pub_key_response["key"])

        EventHandler(exchange="auth_pubkey", routing_key="", type="fanout", callbackFunc=callbackUpdatePubkey)


        models.Base.metadata.create_all(engine)
        bl_consul = BLConsul.get_instance()
        bl_consul.init_and_register(app)
        EventHandler(exchange="delivery_saga", routing_key="", type="fanout", callbackFunc=callbackCheckOrder)
        BusinessLogic.get_instance().set_up_status(True)

        return app
