import json

from utils.EventPublisher import EventPublisher
from .businessLogic import BusinessLogic
from utils.EventHandler import EventHandler
from utils.logger import Logger

logger = Logger("delivery_performer")
eventPublisherOrder = EventPublisher("order_performer", "fanout")

def callback(rawMsg):
    jsonMsg = json.loads(rawMsg)
    delivery_dict = BusinessLogic.get_instance().update_delivery(jsonMsg["delivery_id"], "Finished")
    logger.print(msg="Delivery finished. OrderID: " + str(delivery_dict["order_id"]) + " DeliveryID: "+ str(jsonMsg["delivery_id"]) , level=Logger.INFO)
    eventPublisherOrder.send_data(json.dumps({"status": "Delivered", "order_id": delivery_dict["order_id"]}), "")

EventHandler(exchange="delivery_performer", routing_key="", type="fanout", callbackFunc=callback)
