# Index folder
In this folder there are 4 main subfolders
* data-analytics: Here you have all files related with data analytics
* devops: Here you have all files related with devops scripts to automate Cloud infrastructure and services
* microservicios: Here you have all files related with **NEW** microservices
* *pems: Here there are keypairs just for didactic purposes. This folder is used to jump from S1 subnet to S2 subnet with SSH.


### Contributors
* Ander  Bolumburu
* Iker Ocio
* Haritz Saiz
