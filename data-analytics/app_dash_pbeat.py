import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from dash.exceptions import PreventUpdate
import dash_daq as daq
import math
import dash_table

from plotly.graph_objs import *
from plotly import graph_objs as go
from plotly.subplots import make_subplots
from cluster_pbeat import cluster_dbscan_packetbeat, visualize, preproces_packebeat

import threading
import collections


import pandas as pd

#path = "C:/Users/Haritz/Desktop/MU/PBL/GitLab/data-analytics/"
path = "./"
fullpath_pbeat_csv = path+"data/pbeat-out.csv"


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
external_stylesheets = ['assets/sytle.css']

app = dash.Dash(__name__)

import os
def parse_bytes_from_file(path):
    bytes_num = os.path.getsize(path)

    if bytes_num / 1024 > 0:
        unit = "MB"
        number = round(bytes_num / 1024)
        if bytes_num / (1024*1024) > 0:
            unit: "GB"
            number = round(bytes_num / (1024*1024))

    else:
        unit = "B"
        number = bytes_num
    return str(number) + " " + unit

app.layout = html.Div(children=[
    html.Div(
        className="main-wrapper",
        children=[
            html.Div(
                className="left-controller-wrapper",
                children=[
                    html.Div(
                        className="labeled title",
                        children="PACKETBEAT CONTROLLER"
                    ),

                    html.Div(
                        className="section",
                        children=[
                            html.Div(
                                className="input",
                                children=[
                                    html.Div(
                                        className="",
                                        children="Epsilon"
                                    ),
                                    dcc.Input(
                                        id="epsiolon_pbeat",
                                        placeholder='',
                                        type='number',
                                        value='0.3',
                                        debounce=True
                                    )  
                                ]
                            ),
                            html.Div(
                                className="input",
                                children=[
                                    html.Div(
                                        className="",
                                        children="Min samples"
                                    ),
                                    dcc.Input(
                                        id="min_samples_pbeat",
                                        placeholder='',
                                        type='number',
                                        value='10',
                                        debounce=True
                                    )  
                                ]
                            ),    
                        ]
                    ),
                    html.Div(
                        className="section",
                        children=[
                            html.Div(
                                className="input",
                                children=[
                                    html.Div(
                                        className="",
                                        children="X axis"
                                    ),
                                    dcc.Dropdown(
                                        id='x_axis_pbeat',
                                        value="none"
                                    ) 
                                ]
                            ),
                            html.Div(
                                className="input",
                                children=[
                                    html.Div(
                                        className="",
                                        children="Y axis"
                                    ),
                                    dcc.Dropdown(
                                        id="y_axis_pbeat",
                                        value="none"
                                    )  
                                ]
                            ),
                            html.Div(
                                className="input",
                                children=[
                                    html.Div(
                                        className="",
                                        children="Tooltip list"
                                    ),
                                    dcc.Dropdown(
                                        id="tooltip_pbeat",
                                        value="none",
                                        multi=True
                                    )  
                                ]
                            ),    
                        ]
                    ),                     
                ]  
            ),
            html.Div(
                className="visuals-wrapper",
                children=[
                    html.Div(
                        className="beat-container",
                        children=[
                            html.Div(
                                className="data-sumarizer box",
                                children= parse_bytes_from_file(fullpath_pbeat_csv) + " of data analyzed"
                            ),
                            html.Div(
                                className="data",
                                children= [
                                    html.Div(
                                        className="load-wrapper",
                                        children= [
                                            dcc.Loading([
                                                dcc.Graph(
                                                    className="plot", 
                                                    id='pbeat_scatter',
                                                )
                                            ])
                                        ]
                                    ),
                                ]
                            ), 
                            html.Div(
                                className="data-sumarizer box cluster-resume",
                                children=[
                                     html.Div(
                                        className="cluster-title",
                                        children=[
                                            html.Div(
                                                className="",
                                                children="Number of clusters: "
                                            ),
                                            html.Div(
                                                id="pbeat_clusters",
                                                children="-"
                                            ),
                                        ]
                                    ),
                                    html.Div(
                                            className="table",
                                            children=[
                                                dash_table.DataTable(
                                                    id='pbeat_table',
                                                    sort_action="native",
                                                    sort_mode="single",
                                                )
                                            ]
                                    ),
                                ]
                            ),                          
                        ]  
                    )
                ]  
            )
        ]
    ),
])

df_pbeat = None
df_mbeat = None
cluster_pbeat = None
cluster_pbeat = None

xcol_p_g = "none"
ycol_p_g = "none"
tooltip_p_g = []
options_pbeat = None

epsiolon_p_g = 0
min_samples_p_g = 0

event_p = threading.Event()

def add_tarce_to_fig(int_id, fig, x, y, tooltip, outlier=False):
    print(int_id)
    cluster_id = int_id
    if outlier:
        cluster_id = cluster_id -1

    if outlier and int_id == 0:
        print("ploting trace with border: " + str(cluster_id))
        fig.add_trace(go.Scatter(x=x, y=y,
                                mode='markers',
                                name='cluster ' + str(cluster_id),
                                hovertext=tooltip,
                                hoverinfo="text",
                                marker=dict(size=12,
                                            line=dict(width=2,
                                                        color='DarkSlateGrey')
                                           )
                        ))
        return fig

    else:
        fig.add_trace(go.Scatter(x=x, y=y,
                                mode='markers',
                                name='cluster ' + str(cluster_id),
                                hovertext=tooltip,
                                hoverinfo="text",
                        ))
        return fig

def compose_figure(X, Y, tooltip, outlier):
    fig = go.Figure()
    print("PBeat - Adding traces")
    # Add traces
    size = len(X)
    for i in range(len(X)):
        fig = add_tarce_to_fig(i, fig, X[i], Y[i], tooltip[i], outlier=outlier)

            

    fig.update_layout(legend_orientation="h", plot_bgcolor='rgba(0,0,0,0)', paper_bgcolor='rgba(0,0,0,0)', margin=go.layout.Margin(l=10, r=10, t=10, b=10))            
    print("PBeat - End traces")
    return fig


@app.callback(
    [
        Output("pbeat_scatter", "figure"),
        Output("x_axis_pbeat", "options"),
        Output("y_axis_pbeat", "options"),
        Output("tooltip_pbeat", "options"),
    ],   
    [
        Input("epsiolon_pbeat", "value"),
        Input("min_samples_pbeat", "value"),
        Input("x_axis_pbeat", "value"),
        Input("y_axis_pbeat", "value"),
        Input("tooltip_pbeat", "value"),
    ]
)
def update_packetbeat_cluster(epsiolon, min_samples, xcol, ycol, tooltip):
    global xcol_p_g
    global ycol_p_g
    global epsiolon_p_g
    global min_samples_p_g
    global df_pbeat
    global cluster_pbeat
    global options_pbeat
    global tooltip_p_g
    global event_p

    if tooltip == "none":
        tooltip = []

    print("xcol: " + xcol + "   ycol: " + ycol)
    print("xcol_p_g: " + xcol_p_g + "   ycol_p_g: " + ycol_p_g)
    if ycol != "none" and xcol != "none":
        print("valid xcol and ycol")
        if ((xcol == xcol_p_g and ycol != ycol_p_g) or (xcol != xcol_p_g and ycol == ycol_p_g) or (xcol != xcol_p_g and ycol != ycol_p_g)):
            xcol_p_g = xcol
            ycol_p_g = ycol
            num_clusters = len(collections.Counter(cluster_pbeat.labels_))
            X, Y, tooltip_msgs, df_r, clus_r, outlier_r  = visualize(xcol, ycol, df_pbeat, cluster_pbeat, tooltip, num_clusters)
            fig = compose_figure(X, Y, tooltip_msgs, outlier_r)
            print("Its an axis change")
            return fig, options_pbeat, options_pbeat, options_pbeat

        if tooltip_p_g != tooltip:
            print("tooltip change")
            tooltip_p_g = tooltip
            num_clusters = len(collections.Counter(cluster_pbeat.labels_))
            X, Y, tooltip_msgs, df_r, clus_r, outlier_r  = visualize(xcol, ycol, df_pbeat, cluster_pbeat, tooltip, num_clusters)
            fig = compose_figure(X, Y, tooltip_msgs, outlier_r)
            print("Its an axis change")
            return fig, options_pbeat, options_pbeat, options_pbeat

    xcol_p_g = xcol
    ycol_p_g = ycol


    min_samples = int(min_samples)
    epsiolon = float(epsiolon)

    print("epsiolon: " + str(epsiolon) + "   min_samples: " + str(min_samples))
    print("epsiolon_p_g: " + str(epsiolon_p_g) + "   min_samples_p_g: " + str(min_samples_p_g))

    if ((epsiolon == epsiolon_p_g and min_samples != min_samples_p_g) or (epsiolon != epsiolon_p_g and min_samples == min_samples_p_g)or (epsiolon != epsiolon_p_g and min_samples != min_samples_p_g)):

        print("Its a param change (or init) for DBScan")
        #ha cambiado xcol o ycol
        epsiolon_p_g = epsiolon
        min_samples_p_g = min_samples
        X, Y, tooltip_msgs, df, cluster, outlier_r = cluster_dbscan_packetbeat(epsiolon, min_samples, tooltip)
        df_pbeat = df
        cluster_pbeat = cluster
        options = []
        for c in df.columns:
            options.append(dict(
                label=c,
                value=c
            ))
        options_pbeat = options
        fig = compose_figure(X, Y, tooltip_msgs, outlier_r)
        event_p.set()
        event_p.clear()

        return fig, options, options, options

    print("No update")
    raise PreventUpdate

@app.callback(
    Output("pbeat_clusters", "children"),
    [
        Input("epsiolon_pbeat", "value"),
        Input("min_samples_pbeat", "value"),
    ]
)
def update_num_clusters(epsilon, num):
    event_p.wait()
    return str(len(collections.Counter(cluster_pbeat.labels_)))

@app.callback(
    [Output("pbeat_table", "data"), Output('pbeat_table', 'columns')],
    [
        Input("x_axis_pbeat", "value"),
        Input("y_axis_pbeat", "value"),
    ]
)
def update_datetavle(xcol, ycol):
    if ycol != "none" and xcol != "none":
        cols = [
            {"id":"cluster", "name": "cluster"},
            {"id":xcol, "name": xcol},
            {"id":ycol, "name": ycol},
        ]
        data_joined = df_pbeat[["cluster", xcol, ycol]]
        size = len(collections.Counter(cluster_pbeat.labels_))
        clusters = []
        for i in range(size):
            clusters.append(i)
        df_composed = data_joined.groupby(['cluster']).mean()
        df_composed.insert(0, "cluster", clusters)
        df_composed = df_composed.round(2)
        return df_composed.to_dict('records'), cols
    else:
        raise PreventUpdate

if __name__ == '__main__':
    app.run_server(debug=True, port=8050) 
