# Data-analytics folder

In this folder are all files related with Data-analytics.

There is also an available jupyter notebook that explains the (pre)procesing and conclusions taken after applying unsupervised alogrithms to detect anomalies. It explains the results for packet beat data as well as metric beat. The notebook is named ```Data-Analytics.ipynb```

In addition, you can play with Clustering hyperparameters using our two web tools based on two Plotly projects.

To start Plotly, just write next command:

```sh
$ docker-compose up -d plotly_mbeat plotly_pbeat
```

Both servers are in port 8050 and 8051 respectively.

### Contributors
* Ander  Bolumburu
* Iker Ocio
* Haritz Saiz

