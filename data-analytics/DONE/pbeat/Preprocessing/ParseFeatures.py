import numpy as np
import pandas as pd


class ParseFeatures:

    @staticmethod
    def parse_features(df, feature_array, debug=True):

        for f in feature_array:
            for index in range(len(df[f])):
                i = df[f].iloc[index]
                i = str(i)
                if "," in i:
                    i = i.replace(",", "")
                if "." in i:
                    i = i.split(".", 1)[0]
                try:
                    i = int(i)
                    df[f].iloc[index] = i

                except ValueError:
                    # Handle the exception
                    df[f].iloc[index] = str(i)

        return df