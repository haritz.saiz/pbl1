import pandas as pd
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from Preprocessing.datachecking import DataChecking
from sklearn.cluster import DBSCAN
from Preprocessing.featureextraction import FeatureExtraction
from Preprocessing.featureselection import FeatureSelection
import numpy as np
import collections
import datetime
import time

#path = "C:/Users/Haritz/Desktop/MU/PBL/GitLab/data-analytics/"
path = "./"
fullpath_pbeat_csv = path+"data/pbeat-out.csv"
fullpath_mbeat_csv = path+"data/metricbeat.csv"

df_pbeat = None
df_pbeat_standared = None
df_mbeat_cpu = None
df_mbeat = None

threshold_plot = 1000

def timestamp_to_string(num):
    return time.strftime('%Y-%m-%d %H:%M', time.localtime(num))


def parse_bytes(bytes_num):
    bytes_num = int (bytes_num)
    if bytes_num / 1024 >= 1:
        unit = "MB"
        number = round(bytes_num / 1024)
        if bytes_num / (1024*1024) >= 1:
            unit: "GB"
            number = round(bytes_num / (1024*1024))

    else:
        unit = "B"
        number = bytes_num
    return str(number) + " " + unit

def remove_column_from_df(df, column_name):
    return df.drop(columns=[column_name])

def encode_column(df, column_name):
    le = preprocessing.LabelEncoder()
    df[column_name] = le.fit_transform(df[column_name])
    return df

import datetime
def elk_timestamp_to_epoch(elk_timestamp_string):
    splits = elk_timestamp_string.split(' ', 5)
    year = int(splits[2])
    month = 1
    
    day = int(splits[1].split(",",2)[0])
    
    splits3 = splits[4].split(':', 2)
    hour = int(splits3[0])
    minute = int(splits3[1])
    seconds = int(splits3[2].split('.', 1)[0])

    date = datetime.datetime(year, month, day, hour, minute, seconds)
    
    return date.timestamp()


def normalize_timestamps(df):
    print("normalizing timestamp")
    for index in range(len(df["@timestamp"])):
        if index % 10000 == 0:
            print(index)
        old_timestamp = df["@timestamp"].iloc[index]
        timestamp = elk_timestamp_to_epoch(old_timestamp)
        df["@timestamp"].iloc[index] = int(timestamp)
    print("normalizing timestamp END")
    return df



def preproces_packebeat():
    global df_pbeat_standared
    global df_pbeat

    df = pd.read_csv(fullpath_pbeat_csv)

    max_timestamp = 1579631542
    min_timestamp = 1579622401
    print(df.shape)

    #df = df.loc[df['timestamp'] >= min_timestamp]

    df = df.loc[df['source.port'] != 9200]
    df = df.loc[df['destination.port'] != 9200]
    #df = df.loc[df['labels_intrusion_type'] == "micros"]


    print(df.columns)

    df = DataChecking.missig_values(df=df,action="remove", debug=False)
    #df = normalize_timestamps(df)
    df_original = df.copy()
    print(df.shape)

    df = remove_column_from_df(df, "timestamp")
    df = remove_column_from_df(df, "source.ip")
    df = remove_column_from_df(df, "destination.ip")
    

    df = encode_column(df, "labels_intrusion_type")
    df = encode_column(df, "network.transport")
    df = encode_column(df, "network.type")
    for col in df.columns.values:
        df[col] = df[col].astype(str).str.replace(',', '').astype(float)
 
    scaler = StandardScaler()
    df_new = scaler.fit_transform(df)
    df_new = pd.DataFrame(data=df_new, columns=df.columns)

    df_pbeat = df_original
    df_pbeat_standared = df_new

    print("assigning df_pca_pbeat")
    #df_pca = FeatureExtraction.get_PCA(df_copy, 5)
    #print(df_pca.head())
    #df_pca_pbeat = df_pca

def cluster_dbscan_packetbeat(epsilon, min_samples, tooltip):
    df_pca_pbeat_copy = df_pbeat_standared.copy()
    print("clustering df_pca_pbeat: " + str(epsilon) + " " + str(min_samples))
    clustering = DBSCAN(eps=epsilon, min_samples=min_samples).fit(df_pca_pbeat_copy.values)
    print("end clustering df_pca_pbeat")

    np.unique(clustering.labels_)
    print(collections.Counter(clustering.labels_))

    x_column_name = df_pca_pbeat_copy.columns[0]
    y_column_name = df_pca_pbeat_copy.columns[1]

    df_res = df_pbeat.copy()
    df_res.insert(len(df_pbeat.columns), "cluster", clustering.labels_)
    
    num_clusters = len(collections.Counter(clustering.labels_))

    return visualize(x_column_name, y_column_name, df_res, clustering, tooltip, num_clusters)

def visualize(col_X, col_Y, df, cluster_result, tooltip_list, num_clusters, check_outliers_in_cluster=True):
    if col_X not in tooltip_list:
        tooltip_list.append(col_X)

    if col_Y not in tooltip_list:
        tooltip_list.append(col_Y)
    
    if "cluster" not in tooltip_list:
        tooltip_list.append("cluster")

    print("[cluster] [visualize] tooltip_list: " + str(tooltip_list) )
    X = []
    Y = []
    tooltip = []

    if -1 in cluster_result.labels_ and check_outliers_in_cluster:
        start = -1
        outlier = True
    else:
        outlier = False
        start = 0
    for i in range(start, num_clusters):
        df_partial = df.loc[df['cluster'] == i]

        aux_colx = df_partial[[col_X]]
        aux_coly = df_partial[[col_Y]]
        colx = aux_colx.values.flatten().tolist()
        coly = aux_coly.values.flatten().tolist()

        current_tooltip = []

        for j in range(df_partial.shape[0]):

            tooltip_msg = ""
            for f in tooltip_list:
                num = df_partial[f].iloc[j]
                str_num = str(num)
                if "bytes" in f:
                    str_num = parse_bytes(num)
                if "timestamp" in f:
                    str_num = timestamp_to_string(num)
                if "cluster" in f:
                    if outlier:
                        str_num = str(int(num))
                    else:
                        str_num = str(int(num + 1))


                tooltip_msg = "<br>" + tooltip_msg + f + ": " + str_num + "<br>"
            current_tooltip.append(tooltip_msg)

        X.append(colx)
        Y.append(coly)
        tooltip.append(current_tooltip)
    
    
    #sample overpopulated clusters to reduce CPU and MEM from browser

    for i in range(len(X)):
        if len(X[i]) > threshold_plot:
            idx = np.random.choice(np.arange(len(X[i])), threshold_plot, replace=False)
            new_X = []
            new_Y = []
            new_tootlip = []
            for j in idx:
                new_X.append(X[i][j])
                new_Y.append(Y[i][j])
                new_tootlip.append(tooltip[i][j])
            X[i] = new_X
            Y[i] = new_Y
            tooltip[i] = new_tootlip
    
  

    return X, Y, tooltip, df, cluster_result, outlier


preproces_packebeat()
#preproces_metricbeat()