import numpy as np
import pandas as pd
import datetime

#path = "C:/Users/Haritz/Desktop/MU/PBL/GitLab/data-analytics/"
path = "./"
fullpath_pbeat_csv = path+"data/csv-export-m.csv"
fullpath_mbeat_csv = path+"data/metricbeat.csv"
PATH_OUT = path+"data/pbeat-out-m.csv"

def elk_timestamp_to_epoch(elk_timestamp_string):
    splits = elk_timestamp_string.split('-', 2)
    year = int(splits[0])
    month = int(splits[1])
    
    splits2 = splits[2].split('T', 1)
    day = int(splits2[0])
    
    splits3 = splits2[1].split(':', 2)
    hour = int(splits3[0])
    minute = int(splits3[1])
    seconds = int(splits3[2].split('.', 1)[0])

    date = datetime.datetime(year, month, day, hour, minute, seconds)
    
    return date.timestamp()




df = pd.read_csv(fullpath_pbeat_csv)
print("normalizing timestamp")
for index in range(len(df["timestamp"])):
    if index % 10000 == 0:
        print(index)
    old_timestamp = df["timestamp"].iloc[index]
    timestamp = elk_timestamp_to_epoch(old_timestamp)
    df["timestamp"].iloc[index] = timestamp
print("normalizing timestamp END")

df.to_csv(PATH_OUT, encoding='utf-8', index=False)