from sqlalchemy import and_

from .models import Piece
from . import Session, Logger
from .api_client import *
import jwt
import datetime

logger = Logger("businessLogic")

class BusinessLogic():
    __instance = None
    __public_key = None
    __up_status = False

    def __init__(self):
        if BusinessLogic.__instance is not None:
            raise Exception("This class is a singelton")
        else:
            message = "Service initialized"
            logger.print(message, Logger.INFO)
            BusinessLogic.__instance = self

    @staticmethod
    def get_instance():
        if BusinessLogic.__instance is None:
            BusinessLogic()
        return BusinessLogic.__instance

    def decrypt_jwt(self, auth_header):
        args = auth_header.split("Bearer ")
        jwt_token = args[1]
        decoded_token = jwt.decode(jwt_token, BusinessLogic.__public_key, algorithms='RS256')
        return decoded_token

    def set_auth_public_key(self, pubkey):
        BusinessLogic.__public_key = pubkey

    def get_auth_public_key(self):
        return BusinessLogic.__public_key

    def set_up_status(self, status):
        BusinessLogic.__up_status = status
        if status:
            message = "Service up"
            logger.print(message, level=Logger.INFO)

    def get_up_status(self):
        return BusinessLogic.__up_status

    def create_piece(self, type):
        session = Session()
        try:
            piece = Piece()
            piece.order = None
            piece.type = type
            session.add(piece)
            session.commit()
            piece_dict = piece.as_dict()
            session.close()
            return piece_dict
        except KeyError:
            session.rollback()
            session.close()
            return None

    def get_all_pieces(self):
        session = Session()
        orders = session.query(Piece).all()
        session.close()
        return orders

    def get_piece(self, piece_id):
        session = Session()
        piece = session.query(Piece).get(piece_id)
        if not piece:
            session.close()
            return None
        session.close()
        return piece

    def get_manufactured_pieces_by_type(self, type):
        session = Session()
        #piece = session.query(Piece).all().filter_by(name=Piece.STATUS_MANUFACTURED)
        piece = session.query(Piece).filter(Piece.status == Piece.STATUS_MANUFACTURED, Piece.type == type).all()
        if not piece:
            session.close()
            return None
        session.close()
        return piece

    def get_manufactured_pieces(self):
        session = Session()
        piece = session.query(Piece).filter_by(type=Piece.STATUS_MANUFACTURED).all()
        if not piece:
            session.close()
            return None
        session.close()
        return piece

    def update_piece_owner(self, piece_id, order_id):
        session = Session()
        try:
            piece = session.query(Piece).get(piece_id)
            piece.order_id = order_id
            piece.status = piece.STATUS_OWNED
            session.commit()
            piece_dict = piece.as_dict()
            session.close()
            return piece_dict
        except KeyError:
            session.rollback()
            session.close()
            return None

    def update_piece_status(self, piece_id, status):
        session = Session()
        try:
            piece = session.query(Piece).get(piece_id)
            piece.status = status
            session.commit()
            piece_dict = piece.as_dict()
            session.close()
            return piece_dict
        except KeyError:
            session.rollback()
            session.close()
            return None