from random import randint
from time import sleep
from collections import deque
from threading import Thread, Lock, Event
from .api_client import update_order_status
from utils.EventPublisher import EventPublisher
import json
from utils.logger import Logger

logger = Logger("machine")

class Machine(Thread):
    __instance = None


    STATUS_WAITING = "Waiting"
    STATUS_CHANGING_PIECE = "Changing Piece"
    STATUS_WORKING = "Working"
    __stauslock__ = Lock()
    thread_session = None

    def __init__(self):
        if Machine.__instance is not None:
            raise Exception("This class is a singelton")
        else:
            Machine.__instance = self
            Thread.__init__(self)
            self.queue = deque([])
            self.working_piece = None
            self.status = Machine.STATUS_WAITING
            self.instance = self
            self.queue_not_empty_event = Event()
            #self.reload_pieces_at_startup()
            self.start()

    @staticmethod
    def get_instance():
        if Machine.__instance is None:
            Machine()
        return Machine.__instance

    '''
    def reload_pieces_at_startup(self):
        try:
            self.thread_session = Session()
            manufacturing_piece = self.thread_session.query(Piece).filter_by(status=Piece.STATUS_MANUFACTURING).first()
            if manufacturing_piece:
                self.add_piece_to_queue(manufacturing_piece)

            queued_pieces = self.thread_session.query(Piece).filter_by(status=Piece.STATUS_QUEUED).all()
            if queued_pieces:
                self.add_pieces_to_queue(queued_pieces)
            self.thread_session.close()
        except (sqlalchemy.exc.ProgrammingError, sqlalchemy.exc.OperationalError):
            print("Error getting Queued/Manufacturing Pieces at startup. It may be the first execution")
    '''
    def run(self):
        while True:
            self.queue_not_empty_event.wait()
            logger.print("Thread notified that queue is not empty.", level=Logger.INFO)

            while self.queue.__len__() > 0:
                self.instance.create_piece()

            self.queue_not_empty_event.clear()
            logger.print("Lock thread because query is empty.", level=Logger.INFO)

            self.instance.status = Machine.STATUS_WAITING

    def create_piece(self):
        # Get piece from queue
        piece = self.queue.popleft()

        # Machine and piece status updated during manufacturing
        self.working_piece = piece

        # Machine and piece status updated before manufacturing
        self.working_piece_to_manufacturing()

        # Simulate piece is being manufactured
        sleep(randint(5, 6))

        # Machine and piece status updated after manufacturing
        self.working_piece_to_finished()

        self.working_piece = None

    def working_piece_to_manufacturing(self):
        self.status = Machine.STATUS_WORKING

    def working_piece_to_finished(self):
        self.instance.status = Machine.STATUS_CHANGING_PIECE
        event = str(self.working_piece["piece_id"])
        eventPublisher = EventPublisher("store", "fanout")
        eventPublisher.send_data(json.dumps({"event": "piece_finish", "piece_id":event}), "")
        logger.print(msg="event: piece with id " + str(self.working_piece["piece_id"]) + " finished",
                     level=Logger.INFO)


    def add_piece_to_queue(self, piece):
        self.queue.append(piece)
        logger.print("Adding piece to queue: {}".format(piece["piece_id"]), level=Logger.INFO)
        self.queue_not_empty_event.set()

    def remove_pieces_from_queue(self, pieces):
        for piece in pieces:
            if piece["status"] == "Queued":
                self.queue.remove(piece)
                #update_order_status(self.working_piece["piece_id"], "Cancelled")


