from flask import request, jsonify, abort
from flask import current_app as app
from jwt import DecodeError, ExpiredSignatureError
from werkzeug.exceptions import NotFound, InternalServerError, BadRequest, UnsupportedMediaType, Unauthorized, ServiceUnavailable
import traceback
from .models import Log
from .businessLogic import BusinessLogic
from utils.config import Config
from utils.BLConsul import BLConsul

from utils.logger import Logger
logger = Logger("routes")

config = Config.get_instance()
bl_consul = BLConsul.get_instance()

@app.route('/{}'.format(config.SERVICE_NAME), methods=['GET'])
def get_logs():
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "R_DELIVERY" in decoded_token["perms"]:
            logs = BusinessLogic.get_instance().get_all_logs()
            response = jsonify(Log.list_as_dict(logs))
            return response
        else:
            abort(Unauthorized.code)
    except (KeyError, DecodeError, ExpiredSignatureError):
        logger.print_no_log("Token Error", level=Logger.ERROR)
        abort(Unauthorized.code)

@app.route('/{}/<string:level>'.format(config.SERVICE_NAME), methods=['GET'])
def get_logs_by_level(level):
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "R_DELIVERY" in decoded_token["perms"]:
            logs = BusinessLogic.get_instance().get_log_by_level(level)
            response = jsonify(Log.list_as_dict(logs))
            return response
        else:
            abort(Unauthorized.code)
    except (KeyError, DecodeError, ExpiredSignatureError):
        logger.print_no_log("Token Error", level=Logger.ERROR)
        abort(Unauthorized.code)

@app.route('/{}/health'.format(config.SERVICE_NAME), methods=['HEAD', 'GET'])
@app.route('/health', methods=['HEAD', 'GET'])
def health_check():
    if BusinessLogic.get_instance().get_up_status() == False:
        abort(ServiceUnavailable)
    logger.print_no_log("Health called", level=Logger.HEALTH)
    return "OK"

# Error Handling #######################################################################################################
@app.errorhandler(UnsupportedMediaType)
def unsupported_media_type_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(BadRequest)
def bad_request_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(NotFound)
def resource_not_found_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(InternalServerError)
def server_error_handler(e):
    return get_jsonified_error(e)


def get_jsonified_error(e):
    logger.print_no_log(str({"error_code":e.code, "error_message": e.description, "request": request}), level=Logger.ERROR)
    return jsonify({"error_code":e.code, "error_message": e.description}), e.code