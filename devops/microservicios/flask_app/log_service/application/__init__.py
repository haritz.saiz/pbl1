import json
import time
import datetime
import sys
from json import JSONDecodeError

from flask import Flask
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import create_engine

from .models import Log
from utils.config import Config
from .api_client import get_auth_public_key
from utils.BLConsul import BLConsul

engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)
Session = scoped_session(
            sessionmaker(
                autocommit=False,
                autoflush=True,
                bind=engine)
        )

from .businessLogic import BusinessLogic

from utils.logger import Logger

logger = Logger("__init__")

def callback(body):
    jsonMsg = json.loads(body)
    BusinessLogic.get_instance().create_log(jsonMsg['service'], jsonMsg['level'], datetime.datetime.fromtimestamp(float(jsonMsg['date'])), jsonMsg['message'])

def callbackUpdatePubkey(msgRaw):
    jsonMsg = json.loads(msgRaw)
    BusinessLogic.get_instance().set_auth_public_key(jsonMsg["key"])

def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)

    with app.app_context():
        from . import routes
        from . import models
        models.Base.metadata.create_all(engine)

        while True:
            logger.print_no_log("Trying getting Pubkey", level=Logger.CONNECTION)
            pub_key_response = get_auth_public_key()
            if pub_key_response is None:
                logger.print_no_log("Error getting Auth Pubkey", level=Logger.CONNECTION)
                time.sleep(20)
                continue

            logger.print_no_log("Pubkey succesfully getted", level=Logger.CONNECTION)
            break

        BusinessLogic.get_instance().set_auth_public_key(pub_key_response["key"])

        from utils.EventHandler import EventHandler
        bl_consul = BLConsul.get_instance()
        bl_consul.init_and_register(app)
        EventHandler(exchange="auth_pubkey", routing_key="", type="fanout", callbackFunc=callbackUpdatePubkey)
        EventHandler(exchange="log", routing_key="*.*", type="topic", callbackFunc=callback)
        BusinessLogic.get_instance().set_up_status(True)

        return app

