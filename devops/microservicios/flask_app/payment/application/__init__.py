import json
import sys
import time
from json import JSONDecodeError
from random import randint

from flask import Flask
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import create_engine

from utils.EventPublisher import EventPublisher
from utils.config import Config
from .api_client import get_auth_public_key
from utils.logger import Logger
from utils.BLConsul import BLConsul

logger = Logger("__init__")

engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)
Session = scoped_session(
            sessionmaker(
                autocommit=False,
                autoflush=True,
                bind=engine)
        )

from .businessLogic import BusinessLogic


def callbackMsg(rawMsg):
    jsonMsg = json.loads(rawMsg)

    time.sleep(randint(9, 10))


    status = BusinessLogic.get_instance().update_payment(jsonMsg["client_id"], jsonMsg["balance"])

    if status:
        event = "payment_done"
        logger.print(msg="event: payment done for client id "+str(jsonMsg["client_id"]), level=Logger.INFO)
    else:
        logger.print(msg="event: payment error for client id "+str(jsonMsg["client_id"]), level=Logger.INFO)
        event = "payment_error"
    exchange_response = jsonMsg["exchange_response"]
    eventPublisher = EventPublisher(exchange_response, "fanout")
    eventPublisher.send_data(json.dumps({"event": event}), "")

def callbackUpdatePubkey(msgRaw):
    jsonMsg = json.loads(msgRaw)
    BusinessLogic.get_instance().set_auth_public_key(jsonMsg["key"])

def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)

    with app.app_context():
        from . import routes
        from . import models

        while True:
            message = "Trying getting Pubkey"
            logger.print(msg=message, level=Logger.DEBUG)
            pub_key_response = get_auth_public_key()
            if pub_key_response is None:
                message = "Error getting Auth Pubkey"
                logger.print(msg=message, level=Logger.DEBUG)
                time.sleep(20)
                continue

            message = "Pubkey succesfully getted"
            logger.print(msg=message, level=Logger.DEBUG)
            break

        BusinessLogic.get_instance().set_auth_public_key(pub_key_response["key"])
        BusinessLogic.get_instance().set_auth_public_key(pub_key_response["key"])
        from utils.EventHandler import EventHandler
        bl_consul = BLConsul.get_instance()
        bl_consul.init_and_register(app)
        models.Base.metadata.create_all(engine)
        EventHandler(exchange="auth_pubkey", routing_key="", type="fanout", callbackFunc=callbackUpdatePubkey)
        EventHandler(exchange="payment_saga", routing_key="", type="fanout", callbackFunc=callbackMsg)

        BusinessLogic.get_instance().set_up_status(True)
        return app
