import jwt

from utils.logger import Logger
from .models import Payment
from . import Session

logger = Logger("businessLogic")

#eventPublisher = EventPublisher("payment")

class BusinessLogic():
    __instance = None
    __public_key = None
    __up_status = False

    def __init__(self):
        if BusinessLogic.__instance is not None:
            raise Exception("This class is a singelton")
        else:
            message = "Service initialized"
            logger.print(message, Logger.INFO)
            BusinessLogic.__instance = self


    @staticmethod
    def get_instance():
        if BusinessLogic.__instance is None:
            BusinessLogic()
        return BusinessLogic.__instance

    def decrypt_jwt(self, auth_header):
        args = auth_header.split("Bearer ")
        jwt_token = args[1]
        decoded_token = jwt.decode(jwt_token, BusinessLogic.__public_key, algorithms='RS256')
        return decoded_token

    def set_auth_public_key(self, pubkey):
        BusinessLogic.__public_key = pubkey

    def get_auth_public_key(self):
        return BusinessLogic.__public_key

    def set_up_status(self, status):
        BusinessLogic.__up_status = status
        if status:
            message = "Service up"
            logger.print(message, level=Logger.INFO)

    def get_up_status(self):
        return BusinessLogic.__up_status

    def update_payment(self, user_id, balance):
        session = Session()
        try:
            payment = session.query(Payment).get(user_id)
            if not payment:
                #eventPublisher.send_data(data="PUT Error", routing_key="")
                return None
            if payment.balance - balance < 0:

                return None
            payment.balance -= balance
            session.commit()
            session.close()
            #eventPublisher.send_data(data="PUT " + str(payment.as_dict()), routing_key="")
            return payment

        except KeyError:
            session.rollback()
            session.close()
            return None

    def create_payment(self, user_id, balance):
        session = Session()
        new_payment = None
        try:
            new_payment = Payment(
                user_id=user_id,
                balance=balance
            )
            session.add(new_payment)
            session.commit()
            new_payment_dict = new_payment.as_dict()
            session.close()
            return new_payment_dict
        except KeyError:
            session.rollback()
            session.close()
            return None

    def get_payment(self, user_id):
        session = Session()
        payment = session.query(Payment).get(user_id)
        if not payment:
            session.close()
            return None
        session.close()
        return payment