import json

from utils.logger import Logger
from .models import Order
from utils.EventPublisher import EventPublisher
from .businessLogic import BusinessLogic
from utils.EventHandler import EventHandler

logger = Logger("order_performer")

eventPublisherDelivery = EventPublisher("delivery_performer", "fanout")

def callback(rawMsg):
    logger.print("Order callback: " + str(rawMsg), level=Logger.INFO)
    jsonMsg = json.loads(rawMsg)
    status = jsonMsg["status"]
    if status == "Created":
        BusinessLogic.get_instance().update_order(int(jsonMsg["order_id"]), Order.STATUS_CREATED)
        order = BusinessLogic.get_instance().get_order(int(jsonMsg["order_id"]))
        logger.print('Starting delivery 2', Logger.INFO)
        eventPublisherDelivery.send_data(json.dumps({
            "delivery_type": "finish_order",
            "delivery_id": order.delivery_id,
            "exchange_response": "order_saga",
            "order_id": order.id,
        }), "")

    elif status == "Delivered":
        logger.print('event: delivery_finish', level=Logger.INFO)
        BusinessLogic.get_instance().update_order(int(jsonMsg["order_id"]), Order.STATUS_DELIVERED)
        BusinessLogic.get_instance().update_order(int(jsonMsg["order_id"]), Order.STATUS_FINISHED)
    else:
        logger.print('event: not recognized', level=Logger.INFO)


EventHandler(exchange="order_performer", routing_key="", type="fanout", callbackFunc=callback)
