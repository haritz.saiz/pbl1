import json

from utils.logger import Logger
from utils.EventPublisher import EventPublisher
from .state import State
from .businessLogic import BusinessLogic
from .models import Order
from .api_client import create_piece, get_delivery_id, update_delivery_status
import datetime

eventPublisherPayment = EventPublisher("payment_saga", "fanout")
eventPublisherDelivery = EventPublisher("delivery_saga", "fanout")
eventPublisherStore = EventPublisher("store_saga", "fanout")

logger = Logger("saga_states")

class OrderOrderedState(State):

    def __init__(self, order, address, sagas_id):

        message = "SAGAS: Sagas in OrderOrderedState, starting Order with ID: " + str(order.id)
        logger.print(msg=message, level=Logger.INFO)
        bl = BusinessLogic.get_instance()
        bl.update_sagas(sagas_id, self.__repr__())
        self.order = order
        self.address = address
        self.sagas_id = sagas_id

        bl.update_lock(order.id, True)
        eventPublisherPayment.send_data(json.dumps({
            "exchange_response": "order_saga",
            "balance": order.number_of_pieces * 10,
            "client_id": order.client_id
        }), "")


    def on_event(self, event):
        if event == 'payment_done':
            message = "SAGAS: Sagas in OrderOrderedState, changing state of order with ID: " + str(
                self.order.id) + " to OrderPendingDeliveryState"
            logger.print(msg=message,  level=Logger.INFO)
            BusinessLogic.get_instance().update_order(self.order.id, Order.STATUS_PAYED)
            return OrderPendingDeliveryState(self.order, self.address, self.sagas_id)
        else:
            message = "SAGAS: Sagas in OrderOrderedState, changing state of order with ID: " + str(
                    self.order.id) + " to OrderRejectedState. Reason: NOT ENOUGH MONEY"
            logger.print(msg=message, level=Logger.INFO)
            BusinessLogic.get_instance().update_order(self.order.id, Order.STATUS_CANCELED_NO_MONEY)
            return OrderRejectedState(self.order, self.sagas_id)
        return self


class OrderPendingDeliveryState(State):

    def __init__(self, order, address, sagas_id):
        eventPublisherDelivery.send_data(json.dumps({
            "delivery_type": "approve_order",
            "exchange_response": "order_saga",
            "address": address,
            "client_id": order.client_id,
            "order_id": order.id
        }), "")
        self.order = order
        self.sagas_id = sagas_id
        bl = BusinessLogic.get_instance()
        bl.update_sagas(sagas_id, self.__repr__())
        message = "SAGAS: Sagas in OrderPendingDeliveryState, starting delivery in Order with ID: " + str(order.id)
        logger.print(msg=message, level=Logger.INFO)

    def on_event(self, event):
        if event["status"] == 'delivery_done':
            self.order.delivery_id = event["delivery_id"]
            bl = BusinessLogic.get_instance()
            bl.update_delivery_id(self.order.id, self.order.delivery_id)
            message = "SAGAS: Sagas in OrderPendingDeliveryState, changing state of order with ID: " + str(
                    self.order.id) + " to OrderApprovedState"
            logger.print(msg=message,  level=Logger.INFO)
            BusinessLogic.get_instance().update_order(self.order.id, Order.STATUS_APPROVED)
            return OrderApprovedState(self.order, self.sagas_id)
        else:
            message = "SAGAS: Sagas in OrderPendingDeliveryState, changing state of order with ID: " + str(
                    self.order.id) + " to OrderRefoundState. Reason: ZIP code incorrect"
            logger.print(msg=message,  level=Logger.INFO)
            BusinessLogic.get_instance().update_order(self.order.id, Order.STATUS_CANCELED_BAD_ZIP_CODE)

            return OrderRefoundState(self.order, self.sagas_id)
        return self


class OrderApprovedState(State):

    def __init__(self, order, sagas_id):
        self.order = order
        bl = BusinessLogic.get_instance()
        bl.update_sagas(sagas_id, self.__repr__())
        message = "SAGAS: Sagas in OrderApprovedState, sending request to store of Order with ID: " + str(order.id)
        logger.print(msg=message, level=Logger.INFO)

        eventPublisherStore.send_data(json.dumps({
            "order_id": order.id,
            "number_of_pieces": order.number_of_pieces,
            "type": order.type,
        }), "")

        self.sagas_id = sagas_id


class OrderRefoundState(State):

    def __init__(self, order, sagas_id):
        eventPublisherPayment.send_data(json.dumps({
            "exchange_response": "order_saga",
            "balance": -(order.number_of_pieces * 10),
            "client_id": order.client_id
        }), "")
        bl = BusinessLogic.get_instance()
        bl.update_sagas(sagas_id, self.__repr__())
        message = "SAGAS: Sagas in OrderRefoundState, starting refunding of money to auth " + str(
            order.client_id) + " in Order with ID: " + str(order.id)
        logger.print(msg=message,  level=Logger.INFO)

        self.order = order
        self.sagas_id = sagas_id

    def on_event(self, event):
        if event == 'payment_done':
            message = "SAGAS: Sagas in OrderRefoundState, successful refunding of money to auth " + str(
                    self.order.client_id) + " in Order with ID: " + str(self.order.id)
            logger.print(msg=message, level=Logger.INFO)
            return OrderRejectedState(self.order, self.sagas_id)
        return self


class OrderRejectedState(State):

    def __init__(self, order, sagas_id):
        bl = BusinessLogic.get_instance()
        bl.get_instance().update_order(order.id, Order.STATUS_REJECTED)
        bl.update_sagas(sagas_id, self.__repr__())
        message = "SAGAS: Sagas in OrderRejectedState, rejecting Order with ID: " + str(
            order.id) + " and delivery with ID: " + str(order.delivery_id) + " in case it was created"
        logger.print(msg=message,  level=Logger.INFO)

        # Returns NOT FOUND if the delivery is not created
        update_delivery_status(order.delivery_id, "Rejected")
        self.sagas_id = sagas_id

#######################################################################################################################
# CANCELL SAGA STATES
#######################################################################################################################

class OrderCancellRequestState(State):

    def __init__(self, order_id, sagas_id, saga):
        self.saga = saga
        self.sagas_id = sagas_id
        bl = BusinessLogic.get_instance()
        bl.update_sagas(sagas_id, self.__repr__())
        order = bl.get_order(order_id)

        if order.status in Order.CANCELLABLE_STATUS:
            if order.STATUS_INIT:
                saga.state = OrderNotCancellableState(order_id, self.sagas_id, saga)
            else:
                saga.state = OrderPostPaymentState(order_id, self.sagas_id, saga)

        else:
            saga.state = OrderNotCancellableState(order_id, self.sagas_id, saga)

class OrderPrePaymentState(State):

    def __init__(self, order_id, sagas_id, saga):
        self.sagas_id = sagas_id
        bl = BusinessLogic.get_instance()
        bl.update_sagas(sagas_id, self.__repr__())

class OrderPostPaymentState(State):

    def __init__(self, order_id, sagas_id, saga):
        self.sagas_id = sagas_id
        bl = BusinessLogic.get_instance()
        bl.update_sagas(sagas_id, self.__repr__())

class OrderUnasignPiecesState(State):

    def __init__(self, order_id, sagas_id, saga):
        self.sagas_id = sagas_id
        bl = BusinessLogic.get_instance()
        bl.update_sagas(sagas_id, self.__repr__())


class OrderCancelledState(State):

    def __init__(self, order_id, sagas_id, saga):
        self.sagas_id = sagas_id
        bl = BusinessLogic.get_instance()
        bl.update_sagas(sagas_id, self.__repr__())
        saga.state_saga = True
        bl.update_lock(order_id, False)
        saga.state_saga_event.set()

class OrderNotCancellableState(State):

    def __init__(self, order_id, sagas_id, saga):
        self.sagas_id = sagas_id
        bl = BusinessLogic.get_instance()
        bl.update_sagas(sagas_id, self.__repr__())
        saga.state_saga = False
        bl.update_lock(order_id, False)
        saga.state_saga_event.set()

# End of our states.
