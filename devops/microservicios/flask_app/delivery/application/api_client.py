from json import JSONDecodeError

import requests, json
from os import environ
from utils.BLConsul import BLConsul
from utils.config import Config
from flask import jsonify

from utils.logger import Logger

logger = Logger("api_client")

config = Config.get_instance()
bl_consul = BLConsul.get_instance()


def update_order(order_id, status):
    order_data = {
        "status": status
    }

    headers = {'Content-type': 'application/json'}
    logger.print("update order status: " + str(status) + " with ID: "+str(order_id), level=Logger.DEBUG)
    # order_response = requests.put(url=environ.get("ORDER_IP") + "/order/"+ str(order_id),
    #                               data=json.dumps(order_data),
    #                               headers=headers,
    #                               verify=False)
    order_response = requests.put(url="https://"+environ.get("HAPROXY_IP") + ":"+environ.get("HAPROXY_PORT")+ "/order/"+ str(order_id),
                                  data=json.dumps(order_data),
                                  headers=headers,
                                  verify=environ.get("API_CA_LOCATION"))
    return order_response

def get_auth_public_key():

    # request = requests.get(url=environ.get("AUTH_IP")+"/auth/publickey",
    #                        verify=False)

    #url = "https://"+environ.get("HAPROXY_IP")+ ":"+environ.get("HAPROXY_PORT")+ "/auth/publickey"
    #print(url)
    #request = requests.get(url="https://"+environ.get("HAPROXY_IP")+ ":"+environ.get("HAPROXY_PORT")+ "/auth/publickey",
                           #verify=environ.get("API_CA_LOCATION"))

    ret_message, status_code = external_service_response("auth", "publickey")
    if status_code != 200:
        return None
    json_tree = json.loads(ret_message)
    resp = json_tree["response"]
    logger.print("get_auth_public_key response: " + str(resp), level=Logger.DEBUG)

    json_tree = json.loads(resp)
    return json_tree


def external_service_response(external_service_name, path):
    service = bl_consul.get_service(external_service_name)
    service['Name'] = external_service_name
    if service['Address'] is None or service['Port'] is None:
        ret_message = "The service does not exist or there is no healthy replica"
        status_code = 404
    else:
        service['Path'] = path
        ret_message, status_code = call_external_service(service)
    return ret_message, status_code


def call_external_service(service):
    url = "http://{host}:{port}/{service}/{path}".format(
        host=service['Address'],
        port=service['Port'],
        service=service['Name'],
        path=service['Path']
    )
    response = requests.get(url)
    if response:
        ret_message = json.dumps({
            "caller": config.SERVICE_NAME,
            "callerURL": "{}:{}".format(config.IP, config.PORT),
            "answerer": service['Name'],
            "answererURL": "{}:{}".format(service['Address'], service['Port']),
            "response": response.text,
            "status_code": response.status_code
        })
        status_code = response.status_code
    else:
        ret_message = "Could not get message"
        status_code = 500
    return ret_message, status_code