import datetime
import json
from .config import Config


class Logger:
    DEBUG = "DEBUG"
    INFO = "INFO"
    ERROR = "ERROR"
    HEALTH = "HEALTH"
    CONNECTION = "CONNECTION"

    LEVEL_0 = [DEBUG, INFO, ERROR, HEALTH, CONNECTION]
    LEVEL_1 = [DEBUG, INFO, ERROR]
    LEVEL_2 = [INFO, ERROR]

    def __init__(self, location):
        self.location = location

    def allowedLogLevel(self, level):
        #Comprueba si debe ser printeado y loggeado en base a la propiedad "LOG_LEVEL" del .env
        if Config.LOG_LEVEL == 0:
            return level in Logger.LEVEL_0
        elif Config.LOG_LEVEL == 1:
            return level in Logger.LEVEL_1
        else:
            return level in Logger.LEVEL_2



    def print_no_log(self, msg, level):
        if self.allowedLogLevel(level):
            print("__print no log__ => [ " + Config.SERVICE_NAME_UPPERCASE + " ] [ " + level + " ] [ " + self.location + " ] " + msg)

    def print(self, msg, level):
        if self.allowedLogLevel(level):
            from .EventPublisher import EventPublisher

            print("[ " + Config.SERVICE_NAME_UPPERCASE + " ] [ "+level+" ] [ " + self.location + " ] " + msg)
            if level != Logger.HEALTH:
                ep_log = EventPublisher(exchange="log", type="topic")
                jsonMsg = {
                    "service": Config.SERVICE_NAME_UPPERCASE,
                    "level": level,
                    "date": str(datetime.datetime.timestamp(datetime.datetime.now())),
                    "message": msg
                }
                ep_log.send_data(data=json.dumps(jsonMsg), routing_key=Config.SERVICE_NAME_UPPERCASE + "." + level)
                ep_log.close()