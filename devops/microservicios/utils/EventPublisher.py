#!/usr/bin/env python
import time

import pika
from pika import exceptions
from os import environ
import ssl

from .config import Config

from .logger import Logger

logger = Logger("Event Publisher")

class EventPublisher():
    def __init__(self,exchange, type):
        self.open_conection(exchange, type)

    def open_conection(self, exchange, type):
        while True:
            try:
                logger.print_no_log("Trying rabbit connection", level=Logger.CONNECTION)
                context = ssl.create_default_context(
                    cafile=environ.get("RABBITMQ_CA_CERT_LOCATION"))
                context.load_cert_chain(environ.get("RBT_CERT_LOCATION"),
                                        environ.get("RBT_KEY_LOCATION"))
                ssl_options = pika.SSLOptions(context, "rabitmq")

                self.connection = pika.BlockingConnection(pika.ConnectionParameters(host=environ.get("RABBITMQ_IP"),
                                                                                    port=environ.get("RABBITMQ_PORT_HTTPS"),
                                                                                    ssl_options=ssl_options))
            except exceptions.AMQPConnectionError:
                logger.print_no_log("Error connecting to RMQ", level=Logger.CONNECTION)
                time.sleep(5)
                continue

            logger.print_no_log("Connection established", level=Logger.CONNECTION)
            break

        self.channel = self.connection.channel()
        self.type = type
        self.channel.exchange_declare(exchange=exchange, exchange_type=self.type, durable=True)
        self.exchange = exchange

    def send_data(self, data, routing_key):
        data = str(data)
        try:
            self.channel.basic_publish(exchange=self.exchange,
                                  routing_key=routing_key,
                                  body=data,
                                  properties=pika.BasicProperties(
                                       delivery_mode=2,  # make message persistent
                                  ))

            logger.print_no_log("[x] Sent " + data, level=Logger.CONNECTION)
        except exceptions.AMQPConnectionError:
            logger.print_no_log("Rabbit MQ closed. Reopening ...", level=Logger.CONNECTION)
            self.open_conection(self.exchange, self.type)
            self.send_data(data, routing_key)

    def close(self):
        self.connection.close()
