from os import environ
import netifaces as ni
from dotenv import load_dotenv

# Only needed for developing, on production Docker .env file is used
# load_dotenv()


class Config:

    def get_ip(self):
        if self.HOST_IP:
            Config.IP = self.HOST_IP
            print(Config.IP)
        else:
            print("HOST IP NOT DEFINED")
            exit(1)
        #ifaces = ni.interfaces()
        #if "br-ca1e5a751726" in ifaces:  # this is for my specific iface for debugging.
        #    Config.IP = Config.get_ip_iface("br-ca1e5a751726")
        #    #print("cond1")
        #elif "eth0" in ifaces:  # this is the default interface in docker
        #    Config.IP = Config.get_ip_iface("eth0")
            #print("cond2")
        #else:
        #    Config.IP = "127.0.0.1"
            #print("else")

    """Set Flask configuration vars from .env file."""
    # Database
    SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI")
    SQLALCHEMY_TRACK_MODIFICATIONS = environ.get("SQLALCHEMY_TRACK_MODIFICATIONS")
    CONSUL_IP = environ.get("CONSUL_HOST")
    SERVICE_ID = ""
    HOST_IP = environ.get("HOST_IP", "")
    PORT = int(environ.get("GUNICORN_PORT"))
    SERVICE_NAME = environ.get("SERVICE_NAME")
    SERVICE_NAME_UPPERCASE = environ.get("SERVICE_NAME").upper()
    LOG_LEVEL = int(environ.get("LOG_LEVEL"))
    IP = None

    __instance = None

    @staticmethod
    def get_instance():
        if Config.__instance is None:
            Config()
        return Config.__instance

    def __init__(self):
        """ Virtually private constructor. """
        if Config.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            self.get_ip()
            Config.__instance = self

        Config.SERVICE_ID = Config.SERVICE_NAME + "-" + Config.SERVICE_NAME + "1"



    @staticmethod
    def get_ip_iface(iface):
        return ni.ifaddresses(iface)[ni.AF_INET][0]['addr']
