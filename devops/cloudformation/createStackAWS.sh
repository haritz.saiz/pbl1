#!/bin/bash


# https://<bucket-name>.amazonaws.com/<key>
# https://maccautomatizacion.s3.amazonaws.com/vpc.json
#https://plantillastaller1.s3.amazonaws.com/vpc.json

if [ $# -eq 2 ]; then
    #region=$1
    stackName=$1
    s3Url=$2

    echo '## Obtaining keypair name...'
    #paramSSHKey=$(aws ec2 describe-key-pairs --query "KeyPairs[0].KeyName" --output text)
    #paramSSHKey=lambda
    echo '## Creating stack...'
    echo 
    aws cloudformation create-stack --stack-name $stackName \
        --template-url $s3Url

    echo '## Waiting for stack creation...'
    while [[ `aws cloudformation describe-stacks --stack-name ${stackName} --query Stacks[0].StackStatus` != *"COMPLETE"* ]]
    do
        sleep 10
        echo '## Waiting for stack creation...'
    done
    aws cloudformation describe-stacks --stack-name $stackName --query Stacks[0]
    # y mirar conel curl si esta todo bien

else
    echo "Error, el numero de parametros introducido no es correcto"
    echo "Utilizar: ./createAWSStack.sh stackName s3Url"
fi

