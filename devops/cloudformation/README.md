# Cloudformation folder
In this folder it will be explained how to deploy an Stack, remove it, stop machines or start it with a tag given.
[![AWS architecture](https://imgur.com/T99PuSt.png)](https://imgur.com/T99PuSt.png)

### Requirements
* awscli installed with "*apt-get install awscli*"
* aws configured on a Linux machine with AWS credentials
* A keypair created on your AWS account


### Files
 * createS3.sh to create an S3 bucket or upload a cloudformation file
 * createStackAWS.sh to create a Cloudformation stack
 * deleteStackAWS.sh to delete a Cloudformation stack
 * start-ec2.sh to start multiple ec2 instances with a tag given
 * stop-ec2.sh to stop multiple ec2 instances with a tag given

### How to start
First of all, you need to open vpc.yaml file and change the 7th line
```sh
from    Default: POPBL1 
to      Default: YourCreatedKeypair
```

Then create a uniqued name S3 bucket and upload Cloudformation file
```sh
$ ./createS3.sh -m uniqueS3nameBucket
$ ./createS3.sh -b uniqueS3nameBucket -n vpc.yaml
```

Start creating the Cloudformation stack with next commands
```sh
$ ./createStackAWS.sh cloudformationStackName https://uniqueS3nameBucket.s3.amazonaws.com/vpc.yaml
```

Now you can check how your Cloudformation stack is creating from AWS control panel or just waiting the next output
```sh
{
    "DisableRollback": false,
    "StackId": "arn:aws:cloudformation:us-east-1:162358430615:stack/iocio005vpc/29023500-3f9c-11ea-8522-0a726edfb5e3",
    "RollbackConfiguration": {},
    "DriftInformation": {
        "StackDriftStatus": "NOT_CHECKED"
    },
    "Description": "POPBL Cloud Formation template - Iker, Haritz, Ander",
    "NotificationARNs": [],
    "StackStatus": "CREATE_COMPLETE",
    "CreationTime": "2020-01-25T17:57:32.729Z",
    "EnableTerminationProtection": false,
    "Parameters": [
        {
            "ParameterKey": "KeyName",
            "ParameterValue": "POPBL1"
        }
    ],
    "Tags": [],
    "StackName": "iocio005vpc",
    "Outputs": [
        {
            "OutputValue": "54.198.167.196",
            "Description": "connect via SSH as user ec2-user",
            "OutputKey": "Honeypot1PublicName"
        },
        {
            "OutputValue": "35.173.254.91",
            "Description": "connect via SSH as user ec2-user. Kibana to https and port 443",
            "OutputKey": "VDPublicName"
        },
        {
            "OutputValue": "3.80.199.175",
            "Description": "connect via SSH as user ec2-user. Vault port 8200. HaProxy 14001",
            "OutputKey": "ApiGatewayHostPublicName"
        },
        {
            "OutputValue": "10.0.2.10",
            "Description": "connect via SSH as user ec2-user",
            "OutputKey": "MicroservicesPublicName"
        },
        {
            "OutputValue": "18.232.102.106",
            "Description": "connect to consul via port 8500",
            "OutputKey": "NatServerHostPublicName"
        }
    ]
}
```

### Start/Stop instances

All EC2 instances have its own tag to identify them. They have a common tag in order to stop/start them with a command like

```sh
$ ./stop-ec2.sh us-east-1 System popbl
or
$ ./start-ec2.sh us-east-1 System popbl
```

It is possible to start/stop some of them with their own tag.
All EC2 instances are identified by:
* EC2 Name: its own name
* Subnet name: [S1|S2|S3|S4] all ec2 instances from this subnet
* Type name: [microservices|honeypot|visualanalytics|machinelearning]
* System: all of them

### How to delete created stack
You can delete created stack just with the next command
```sh
$ ./deleteStackAWS.sh cloudformationStackName
```



### Contributors
* Ander  Bolumburu
* Iker Ocio
* Haritz Saiz
