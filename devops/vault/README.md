# Vault folder
In this folder a Dockerized Vault can be found.

To startup, start next command:

```sh
$ docker-compose up -d vault
```

Vault has been created with a root master key and a root token.
Those tokens are in *keys.json* file to unseal and init the managemt gui.

This is just for didactic purposes. keys.json must not be in a Git repository. Is totally unsecure.

### Contributors
* Ander  Bolumburu
* Iker Ocio
* Haritz Saiz
