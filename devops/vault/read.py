import hvac
client = hvac.Client()

# The following path corresponds, when combined with the mount point, to a full Vault API route of "v1/secretz/hvac"
mount_point = 'cubbyhole'
secret_path = 'defaultpath'

read_secret_result = client.secrets.kv.v1.read_secret(
    path=secret_path,
    mount_point=mount_point,
)
print('The "psst" key under the secret path ("/v1/secret/hvac") is: {psst}'.format(
    psst=read_secret_result['data']['username'],
))
