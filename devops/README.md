# Devops folder
In this folder there are 4 main subfolders
* Cloudformation: Here you have all files related with Cloudformation automatization
* elk-monitoring: Here you have all files related with elk-monitoring/honeypots automatic deployment
* microservicios: Here you have all files related with microservices automatic deployment
* vault: Here you have all files related with vault automatic deployment

You can see also 2 files called *get_secret.py* and *get_secret.sh*.
The first one is to get a secret from Vault using Python and the second one is to get a secret from Vault with Vault-cli.
These files are used by EC2 hosts on deployment time.

### Contributors
* Ander  Bolumburu
* Iker Ocio
* Haritz Saiz
