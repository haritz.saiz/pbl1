# ELK-Monitoring folder

In this folder are all files related with ELK Monitoring.

To startup Logstash + Elastic + Kibana, go to logstash-elastic-kibana folder and start next command:

```sh
$ docker-compose up -d elasticsearch kibana logstash
```

To startup Honeypots with Filebeat, Metricbeat and Packetbeat, go to honeybeats folder and start next command:
```sh
$ docker-compose up -d cowrie dionaea filebeat metricbeat packetbeat
```


### Contributors
* Ander  Bolumburu
* Iker Ocio
* Haritz Saiz

