#!/bin/bash

arr=(logstash kibana es)

for i in "${arr[@]}"
do

if [[ -z $i ]]; then
    echo "No container specified"
    exit 1
fi

if [[ "$(docker ps -aq -f name=^/${i}$ 2> /dev/null)" == "" ]]; then
    echo "Container \"$i\" does not exist, exiting."
    exit 1
fi

log=$(docker inspect -f '{{.LogPath}}' $i 2> /dev/null)
truncate -s 0 $log

done




